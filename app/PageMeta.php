<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageMeta extends Model
{
    protected $table = 'pages_meta';

    public function Page()
    {
    	return $this->belongsTo('App\Page');
    }
}
