<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = ['name'];

    public function metas()
    {
    	return $this->hasMany('App\PageMeta');
    }

    public function getMeta($meta)
    {
    	$meta = $this->metas()->where('name', $meta)->first();
    	if (!$meta) {
    		return null;
    	}
    	return $meta->value;
    }
}
