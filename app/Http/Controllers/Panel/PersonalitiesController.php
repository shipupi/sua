<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Page;
use App\PageMeta;
use App\Personalidad;
use App\Http\Controllers\Controller;

class PersonalitiesController extends Controller
{


    public function __construct()
    {
        $this->page_name = 'personalidad';
        $this->page = Page::where('name', $this->page_name)->first();
        $this->imagesPath = 'assets/personalidades_header/';
    }
    
    public function personalidades()
    {
        $page = $this->page;
        $personalities = Personalidad::all();
        return view('admin.personalidad.personalidades', compact('page', 'personalities'));
    }

    public function titulo()
    {
        $page = $this->page;
        return view('admin.personalidad.titulo', compact('page'));
    }

    public function personalidad($id)
    {
        $page = $this->page;
        $personality = Personalidad::findOrFail($id);
        return view('admin.personalidad.personalidad', compact('page', 'personality'));
    }

    public function personalidadSlider($id)
    {
        $page = $this->page;
        $personality = Personalidad::findOrFail($id);
        return view('admin.personalidad.pers_slider', compact('page', 'personality'));   
    }

    public function header()
    {
        $page = $this->page;
        return view('admin.personalidad.header', compact('page'));
    }

    public function updatePersonality(Request $request)
    {
        $exceptions = ['_token', 'personality_id', 'redirect', 'image', 'header'];
        $input = $request->all();

        $personality = Personalidad::findOrFail($input['personality_id']);
        $image = $request->file('image');
        if ($image) {
            $imageName = $image->getClientOriginalName();
            $pathDirectory = 'assets/personalities/';
            $fullPath = $pathDirectory . $imageName;
            $request->file('image')->move( base_path() . '/public/' . $pathDirectory, $imageName);
            $personality->image = $fullPath;
            $personality->save();
        }

        $image = $request->file('header');
        if ($image) {
            $imageName = $image->getClientOriginalName();
            $pathDirectory = 'assets/personalities/';
            $fullPath = $pathDirectory . $imageName;
            $request->file('header')->move( base_path() . '/public/' . $pathDirectory, $imageName);
            $personality->header = $fullPath;
            $personality->save();
        }

        foreach ($input as $field => $value) {
            if (!in_array($field, $exceptions)) {
                if (!$value) {
                    $value = '';
                }
                $personality->{$field} = $value;
                $personality->save();
            }
        }
        return redirect($request->input('redirect'));

    }

    
    public function updateSlider($id, Request $request)
    {

        $image = $request->file('image');
        if (!$image) {
            return 'could not puload empty image';
            return redirect($request->input('redirect'));
        }
        $imageName = $image->getClientOriginalName();
        $pathDirectory = 'assets/personalities/slider/';
        $fullPath = $pathDirectory . $imageName;
        $request->file('image')->move( base_path() . '/public/' . $pathDirectory, $imageName);


        $page = $this->page;
        $pers = Personalidad::findOrFail($id);
        $sliderArray = json_decode($pers->slider_images);
        array_push($sliderArray, $fullPath);
        $pers->slider_images = json_encode($sliderArray);
        $pers->save();
        return redirect($request->input('redirect'));
    }

    public function update(Request $request)
    {

        $exceptions = ['_token', 'page_id', 'redirect', 'personalidad_header'];
        $input = $request->all();

        $images = ['personalidad_header'];
        foreach ($images as $i) {
            $image = $request->file($i);
             if ($image) {
                // Image exists, uploading
                $imageName = $image->getClientOriginalName();
                $pathDirectory = $this->imagesPath;
                $fullPath = $pathDirectory . $imageName;
                $request->file($i)->move( base_path() . '/public/' . $pathDirectory, $imageName);
                $meta = PageMeta::where('name', $i)->where('page_id', $input['page_id'])->first();
                if (!$meta) {
                    $meta = new PageMeta();
                    $meta->name = $i;
                    $meta->page_id = $input['page_id'];
                }
                $meta->value = $fullPath;
                $meta->save();

            }
        }

        foreach ($input as $field => $value) {
            if (!in_array($field, $exceptions)) {
                $meta = PageMeta::where('name', $field)->where('page_id', $input['page_id'])->first();
                if (!$meta) {
                    $meta = new PageMeta();
                    $meta->name = $field;
                    $meta->page_id = $input['page_id'];
                }
                if (!$value) {
                    $value = '';
                }
                $meta->value = $value;
                $meta->save();
            }
        }


        return redirect($input['redirect']);
    }

    public function ajaxAction()
    {
        $action = $_POST['action'];
        $page = $this->page;
        if ($action == 'removeSlider') {
            $index = $_POST['index'];
            $id = $_POST['id'];
            $pers = Personalidad::findOrFail($id);
            $sliderArray = json_decode($pers->slider_images);
            array_splice($sliderArray, $index,1);
            $pers->slider_images = json_encode($sliderArray);
            $pers->save();

            echo "ok";
            return;
        }
    }

}
