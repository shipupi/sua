<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Page;
use App\PageMeta;
use App\Http\Controllers\Controller;

class FaqController extends Controller
{


    public function __construct()
    {
        $this->page_name = 'faq';
        $this->page = Page::where('name', $this->page_name)->first();
        $this->imagesPath = 'assets/faq/';
    }

    public function update(Request $request)
    {

        $exceptions = ['_token', 'page_id', 'redirect', 'faq_header'];
        $input = $request->all();

        $images = ['faq_header'];
        foreach ($images as $i) {
            $image = $request->file($i);
             if ($image) {
                // Image exists, uploading
                $imageName = $image->getClientOriginalName();
                $pathDirectory = $this->imagesPath;
                $fullPath = $pathDirectory . $imageName;
                $request->file($i)->move( base_path() . '/public/' . $pathDirectory, $imageName);
                $meta = PageMeta::where('name', $i)->where('page_id', $input['page_id'])->first();
                if (!$meta) {
                    $meta = new PageMeta();
                    $meta->name = $i;
                    $meta->page_id = $input['page_id'];
                }
                $meta->value = $fullPath;
                $meta->save();

            }
        }

        foreach ($input as $field => $value) {
            if (!in_array($field, $exceptions)) {
                $meta = PageMeta::where('name', $field)->where('page_id', $input['page_id'])->first();
                if (!$meta) {
                    $meta = new PageMeta();
                    $meta->name = $field;
                    $meta->page_id = $input['page_id'];
                }
                if (!$value) {
                    $value = '';
                }
                $meta->value = $value;
                $meta->save();
            }
        }


        return redirect($input['redirect']);
    }


    public function titulo()
    {
        $page = $this->page;
        return view('admin.faq.titulo', compact('page'));
    }

    public function questions()
    {
        $page = $this->page;
        return view('admin.faq.questions', compact('page'));
    }

    public function ajaxAction()
    {
        $action = $_POST['action'];
        $page = $this->page;
        if ($action == 'removeSlider') {
            $field = $_POST['field'];
            $index = $_POST['index'];
            $meta = PageMeta::where('name', $field)->where('page_id', $page->id)->first();
            $sliderArray = json_decode($meta->value);
            array_splice($sliderArray, $index,1);
            $meta->value = json_encode($sliderArray);
            $meta->save();

            echo "ok";
            return;
        }
    }

}
