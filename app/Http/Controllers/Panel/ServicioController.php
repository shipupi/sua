<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Page;
use App\PageMeta;
use App\Http\Controllers\Controller;

class ServicioController extends Controller
{


    public function __construct()
    {
        $this->page_name = 'servicio';
        $this->page = Page::where('name', $this->page_name)->first();
        $this->imagesPath = 'assets/servicio/';
    }

    public function header()
    {
        $page = $this->page;
        return view('admin.servicio.header', compact('page'));
    }

    public function titulo()
    {
        $page = $this->page;
        return view('admin.servicio.titulo', compact('page'));
    }


    public function video()
    {
        $page = $this->page;
        return view('admin.servicio.video', compact('page'));
    }
    public function servicio()
    {
        $page = $this->page;
        return view('admin.servicio.servicio', compact('page'));
    }
    public function slider()
    {
        $page = $this->page;
        return view('admin.servicio.slider', compact('page'));
    }

    public function numerada()
    {
        $page = $this->page;
        return view('admin.servicio.numerada', compact('page'));
    }
    public function iconos()
    {
        $page = $this->page;
        return view('admin.servicio.iconos', compact('page'));
    }


    public function updateSlider(Request $request)
    {

        $image = $request->file('image');
        if (!$image) {
            return 'could not upload empty image';
            return redirect($request->input('redirect'));
        }
        $imageName = $image->getClientOriginalName();
        $pathDirectory = 'assets/servicio/slider/';
        $fullPath = $pathDirectory . $imageName;
        $request->file('image')->move( base_path() . '/public/' . $pathDirectory, $imageName);


        $page = $this->page;
        $field = $request->input('field');
        $meta = PageMeta::where('name', $field)->where('page_id', $page->id)->first();
        $sliderArray = json_decode($meta->value);
        array_push($sliderArray, $fullPath);
        $meta->value = json_encode($sliderArray);
        $meta->save();
        return redirect($request->input('redirect'));
    }



    public function update(Request $request)
    {

        $exceptions = ['_token', 'page_id', 'redirect', 'servicio_header'];
        $input = $request->all();

        // Upload de imagenes
        $images = ['servicio_header'];
        foreach ($images as $i) {
            $image = $request->file($i);
             if ($image) {
                // Image exists, uploading
                $imageName = $image->getClientOriginalName();
                $pathDirectory = $this->imagesPath;
                $fullPath = $pathDirectory . $imageName;
                $request->file($i)->move( base_path() . '/public/' . $pathDirectory, $imageName);
                $meta = PageMeta::where('name', $i)->where('page_id', $input['page_id'])->first();
                if (!$meta) {
                    $meta = new PageMeta();
                    $meta->name = $i;
                    $meta->page_id = $input['page_id'];
                }
                $meta->value = $fullPath;
                $meta->save();

            }
        }

        foreach ($input as $field => $value) {
            if (!in_array($field, $exceptions)) {
                $meta = PageMeta::where('name', $field)->where('page_id', $input['page_id'])->first();
                if (!$meta) {
                    $meta = new PageMeta();
                    $meta->name = $field;
                    $meta->page_id = $input['page_id'];
                }
                if (!$value) {
                    $value = '';
                }
                $meta->value = $value;
                $meta->save();
            }
        }


        return redirect($input['redirect']);
    }

    public function ajaxAction()
    {
        $action = $_POST['action'];
        $page = $this->page;
        if ($action == 'removeSlider') {
            $field = $_POST['field'];
            $index = $_POST['index'];
            $meta = PageMeta::where('name', $field)->where('page_id', $page->id)->first();
            $sliderArray = json_decode($meta->value);
            array_splice($sliderArray, $index,1);
            $meta->value = json_encode($sliderArray);
            $meta->save();

            echo "ok";
            return;
        }
    }

}
