<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Page;
use App\PageMeta;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{


    public function __construct()
    {
        $this->page_name = 'home';
        $this->page = Page::where('name', $this->page_name)->first();
    }
    public function slider()
    {
        $page = $this->page;
        return view('admin.home.slider', compact('page'));
    }

    public function sliderSingle($id)
    {
        $page = $this->page;
        $slider = PageMeta::where('name', 'slider_top')->where('page_id', $page->id)->first();
        $slider = json_decode($slider->value);
        return view('admin.home.sliderSingle', compact('page', 'slider', 'id'));
    }

    public function updateSliderSingle(Request $request, $id)
    {
        // dd($request);
        $page = $this->page;
        $slider = PageMeta::where('name', 'slider_top')->where('page_id', $page->id)->first();
        $sliderVal = json_decode($slider->value);
        $sliderItem = $sliderVal[$id];
        $sliderVal[$id]->title = $request->input('titulo');
        $sliderVal[$id]->subtitle = $request->input('subtitle');

        $image = $request->file('image');
        if ($image) {
            $imageName = $image->getClientOriginalName();
            $pathDirectory = 'assets/home/slider/';
            $fullPath = $pathDirectory . $imageName;
            $request->file('image')->move( base_path() . '/public/' . $pathDirectory, $imageName);    
            $sliderVal[$id]->image = $fullPath;
        }


        $slider->value = json_encode($sliderVal);
        $slider->save();
        return redirect($request->input('redirect'));
    }

    public function updateSlider(Request $request)
    {

        $image = $request->file('image');
        $page = $this->page;
        $field = $request->input('field');
        $meta = PageMeta::where('name', $field)->where('page_id', $page->id)->first();
        $sliderArray = json_decode($meta->value);
        if ($image) {
            $imageName = $image->getClientOriginalName();
            $pathDirectory = 'assets/home/slider/';
            $fullPath = $pathDirectory . $imageName;
            $request->file('image')->move( base_path() . '/public/' . $pathDirectory, $imageName);
            array_push($sliderArray, ['image' => $fullPath, 'title' => '', 'subtitle' => '']);
            $meta->value = json_encode($sliderArray);
            $meta->save();
        } else if ($request->input('youtube')) {
            array_push($sliderArray, ['image' => $request->input('youtube'), 'title' => '', 'subtitle' => '']);
            $meta->value = json_encode($sliderArray);
            $meta->save();
        } else {
            return 'No se pudo subir nuevo elemento';
        }
        return redirect($request->input('redirect'));
    }

    public function quienesMain()
    {
        $page = $this->page;
        return view('admin.home.quienesMain', compact('page'));
    }

    public function update(Request $request)
    {

        $exceptions = ['_token', 'page_id', 'redirect'];
        $input = $request->all();

        foreach ($input as $field => $value) {
            if (!in_array($field, $exceptions)) {
                $meta = PageMeta::where('name', $field)->where('page_id', $input['page_id'])->first();
                if (!$meta) {
                    $meta = new PageMeta();
                    $meta->name = $field;
                    $meta->page_id = $input['page_id'];
                }
                if (!$value) {
                    $value = '';
                }
                $meta->value = $value;
                $meta->save();
            }
        }


        return redirect($input['redirect']);
    }


    public function quienes1()
    {
        $page = $this->page;
        return view('admin.home.quienes1', compact('page'));
    }

    public function quienes2()
    {
        $page = $this->page;
        return view('admin.home.quienes2', compact('page'));
    }


    public function quienes3()
    {
        $page = $this->page;
        return view('admin.home.quienes3', compact('page'));
    }


    public function contadores()
    {
        $page = $this->page;
        return view('admin.home.contadores', compact('page'));
    }

    public function ajaxAction()
    {
        $action = $_POST['action'];
        $page = $this->page;
        if ($action == 'removeSlider') {
            $field = $_POST['field'];
            $index = $_POST['index'];
            $meta = PageMeta::where('name', $field)->where('page_id', $page->id)->first();
            $sliderArray = json_decode($meta->value);
            array_splice($sliderArray, $index,1);
            $meta->value = json_encode($sliderArray);
            $meta->save();

            echo "ok";
            return;
        }
    }

}
