<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Page;
use App\PageMeta;
use App\Http\Controllers\Controller;

class ContactoController extends Controller
{


    public function __construct()
    {
        $this->page_name = 'contacto';
        $this->page = Page::where('name', $this->page_name)->first();
        $this->imagesPath = 'assets/contacto/';
    }

    public function update(Request $request)
    {

        $exceptions = ['_token', 'page_id', 'redirect', 'contacto_header'];
        $input = $request->all();

        $images = ['contacto_header'];
        foreach ($images as $i) {
            $image = $request->file($i);
             if ($image) {
                // Image exists, uploading
                $imageName = $image->getClientOriginalName();
                $pathDirectory = $this->imagesPath;
                $fullPath = $pathDirectory . $imageName;
                $request->file($i)->move( base_path() . '/public/' . $pathDirectory, $imageName);
                $meta = PageMeta::where('name', $i)->where('page_id', $input['page_id'])->first();
                if (!$meta) {
                    $meta = new PageMeta();
                    $meta->name = $i;
                    $meta->page_id = $input['page_id'];
                }
                $meta->value = $fullPath;
                $meta->save();

            }
        }

        foreach ($input as $field => $value) {
            if (!in_array($field, $exceptions)) {
                $meta = PageMeta::where('name', $field)->where('page_id', $input['page_id'])->first();
                if (!$meta) {
                    $meta = new PageMeta();
                    $meta->name = $field;
                    $meta->page_id = $input['page_id'];
                }
                if (!$value) {
                    $value = '';
                }
                $meta->value = $value;
                $meta->save();
            }
        }


        return redirect($input['redirect']);
    }


    public function titulo()
    {
        $page = $this->page;
        return view('admin.contacto.titulo', compact('page'));
    }
}
