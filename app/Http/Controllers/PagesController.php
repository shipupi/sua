<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Personalidad;
use Mail;

class PagesController extends Controller
{
    public function home()
    {
    	$home = Page::where('name', 'home')->first();
    	return view('pages.home', compact('home'));
    }

    public function servicio()
    {
        $page = Page::where('name', 'servicio')->first();
        return view('pages.servicio', compact('page'));
    }

    public function faq()
    {
        $page = Page::where('name', 'faq')->first();
        return view('pages.faq', compact('page'));
    }

    public function contact()
    {
        $page = Page::where('name', 'contacto')->first();
        return view('pages.contact', compact('page'));
    }

    public function personalidad()
    {
        $page = Page::where('name', 'personalidad')->first();
        $personalities = Personalidad::all();
        return view('pages.personalidad', compact('page', 'personalities'));
    }

    public function personalidadSingle($id)
    {
    	$page = Page::where('name', 'personalidad')->first();
        $personality = Personalidad::findOrFail($id);
        return view('pages.personalidad-single', compact('page', 'personality'));

    }

    public function contact_store(Request $request)
    {
        // Mandar Mail
        $datos = $request->all();
        Mail::send('emails.contacto', ['datos' => $datos], function ($m) use ($datos) {
            $m->from('seamosunarbolinfo@gmail.com', 'Seamos un arbol');

            $m->to('ail.brianez@gmail.com', 'Seamos un Arbol')->subject('Formululario de contacto');
        });
        return redirect('contacto');
    }
}
