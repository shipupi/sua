<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\PageMeta;


class PanelController extends Controller
{

    
    public function home()
    {
    	$home = Page::where('name', 'home')->first();
    	return view('admin.home', compact('home'));
    }

    public function updateHome(Request $request)
    {
    	$exceptions = ['_token', 'page_id'];
    	$input = $request->all();

    	foreach ($input as $field => $value) {
    		if (!in_array($field, $exceptions)) {
    			$meta = PageMeta::where('name', $field)->where('page_id', $input['page_id'])->first();
    			if (!$meta) {
    				$meta = new PageMeta();
    				$meta->name = $field;
    				$meta->page_id = $input['page_id'];
    			}
                if (!$value) {
                    $value = '';
                }
    			$meta->value = $value;
    			$meta->save();
    		}
    	}

    	return redirect('admin/home');
    }

    public function servicio()
    {
        $servicio = Page::where('name', 'servicio')->first();
        return view('admin.servicio', compact('servicio'));
    }


    public function updateServicio(Request $request)
    {
        $exceptions = ['_token', 'page_id'];
        $input = $request->all();

        foreach ($input as $field => $value) {
            if (!in_array($field, $exceptions)) {
                $meta = PageMeta::where('name', $field)->where('page_id', $input['page_id'])->first();
                if (!$meta) {
                    $meta = new PageMeta();
                    $meta->name = $field;
                    $meta->page_id = $input['page_id'];
                }
                if (!$value) {
                    $value = '';
                }
                $meta->value = $value;
                $meta->save();
            }
        }

        return redirect('admin/servicio');
    }

}
