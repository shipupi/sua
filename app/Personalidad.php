<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personalidad extends Model
{
    protected $table = 'personalities';

    public function getSliderImagesAttribute()
    {
    	if (!$this->attributes['slider_images']) {
    		return json_encode([]);
    	} else {
    		return $this->attributes['slider_images'];
    	}

    }
}
