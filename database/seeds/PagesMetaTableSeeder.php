<?php

use Illuminate\Database\Seeder;
use App\Page;

class PagesMetaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $home = Page::where('name', 'home')->first();
        $servicio = Page::where('name', 'servicio')->first();
        $faq = Page::where('name', 'faq')->first();
        $personalidad = Page::where('name', 'personalidad')->first();
        
        DB::table('pages_meta')->insert([

            // Home
            ['name' => 'slider_top', 'value' => json_encode([
                ['title' => 'test de titulo', 'subtitle' => 'test de subtitle', 'image' => 'assets/home/slider/slider_01.jpg'],
                ['title' => 'test de titulo2', 'subtitle' => 'test de subtitle2', 'image' => 'assets/home/slider/slider_02.jpg'],
            ]), 'page_id' => $home->id],
        	['name' => 'quienes_somos_main_title', 'value' => 'test1', 'page_id' => $home->id],
        	['name' => 'quienes_somos_main_body', 'value' => 'Lorem ipsum largo porque necesito llenar mucho mas espacio', 'page_id' => $home->id],
        	['name' => 'quienes_somos_1_title', 'value' => 'Item 1', 'page_id' => $home->id],
        	['name' => 'quienes_somos_1_body', 'value' => 'Lorem ipsum item 1', 'page_id' => $home->id],
            ['name' => 'quienes_somos_2_title', 'value' => 'Item 2', 'page_id' => $home->id],
        	['name' => 'quienes_somos_2_body', 'value' => 'Lorem Ipsum item 2', 'page_id' => $home->id],
            ['name' => 'quienes_somos_3_title', 'value' => 'Item 3', 'page_id' => $home->id],
            ['name' => 'quienes_somos_3_body', 'value' => 'Lorem ipsum item 3', 'page_id' => $home->id],
        	

            ['name' => 'servicio_desc_izq_title', 'value' => 'Servicio Tradicional', 'page_id' => $servicio->id],
            ['name' => 'servicio_desc_izq_body', 'value' => 'Lorem ipsum servicio tradicional', 'page_id' => $servicio->id],
            ['name' => 'servicio_desc_der_title', 'value' => 'Nueva Mirada ecologica', 'page_id' => $servicio->id],
            ['name' => 'servicio_desc_der_body', 'value' => 'Lorem ipsum Nueva mirada', 'page_id' => $servicio->id],
            ['name' => 'seccion_numerada_title', 'value' => 'Seccion numerada', 'page_id' => $servicio->id],
            ['name' => 'seccion_numerada_1_title', 'value' => '1. One', 'page_id' => $servicio->id],
            ['name' => 'seccion_numerada_1_body', 'value' => 'Nueva Mirada ecologica', 'page_id' => $servicio->id],
            ['name' => 'seccion_numerada_2_title', 'value' => '2. Two', 'page_id' => $servicio->id],
            ['name' => 'seccion_numerada_2_body', 'value' => 'Nueva Mirada ecologica', 'page_id' => $servicio->id],
            ['name' => 'seccion_numerada_3_title', 'value' => '3. Three', 'page_id' => $servicio->id],
            ['name' => 'seccion_numerada_3_body', 'value' => 'Nueva Mirada ecologica', 'page_id' => $servicio->id],


            
            ['name' => 'servicio_slider', 'value' => json_encode([
                ['image' => 'assets/servicios/slider/1.jpg'],
                ['image' => 'assets/servicios/slider/2.jpg'],
                ['image' => 'assets/servicios/slider/3.jpg'],
                ['image' => 'assets/servicios/slider/4.jpg'],
                ['image' => 'assets/servicios/slider/5.jpg'],
            ]), 'page_id' => $servicio->id],
            ['name' => 'iconos_1_title', 'value' => 'Servicios sin costos adicionales', 'page_id' => $servicio->id],
            ['name' => 'iconos_1_body', 'value' => 'Nueva Mirada ecologica', 'page_id' => $servicio->id],
            ['name' => 'iconos_2_title', 'value' => 'Resolvemos tu problema', 'page_id' => $servicio->id],
            ['name' => 'iconos_2_body', 'value' => 'Nueva Mirada ecologica', 'page_id' => $servicio->id],
            ['name' => 'iconos_3_title', 'value' => 'Sin costos de manutención', 'page_id' => $servicio->id],
            ['name' => 'iconos_3_body', 'value' => 'Nueva Mirada ecologica', 'page_id' => $servicio->id],
            ['name' => 'iconos_4_title', 'value' => 'Trabajamos con todas las cías de seguros', 'page_id' => $servicio->id],
            ['name' => 'iconos_4_body', 'value' => 'Nueva Mirada ecologica', 'page_id' => $servicio->id],
            ['name' => 'iconos_5_title', 'value' => 'Precio para independientes', 'page_id' => $servicio->id],
            ['name' => 'iconos_5_body', 'value' => 'Nueva Mirada ecologica', 'page_id' => $servicio->id],
            ['name' => 'iconos_6_title', 'value' => 'Respaldo de S.F.I.', 'page_id' => $servicio->id],
            ['name' => 'iconos_6_body', 'value' => 'Nueva Mirada ecologica', 'page_id' => $servicio->id],


            ['name' => 'faq_1_title', 'value' => 'Servicios sin costos adicionales', 'page_id' => $faq->id],
            ['name' => 'faq_1_body', 'value' => 'Nueva Mirada ecologica', 'page_id' => $faq->id],
            ['name' => 'faq_2_title', 'value' => 'Resolvemos tu problema', 'page_id' => $faq->id],
            ['name' => 'faq_2_body', 'value' => 'Nueva Mirada ecologica', 'page_id' => $faq->id],
            ['name' => 'faq_3_title', 'value' => 'Sin costos de manutención', 'page_id' => $faq->id],
            ['name' => 'faq_3_body', 'value' => 'Nueva Mirada ecologica', 'page_id' => $faq->id],
            ['name' => 'faq_4_title', 'value' => 'Trabajamos con todas las cías de seguros', 'page_id' => $faq->id],
            ['name' => 'faq_4_body', 'value' => 'Nueva Mirada ecologica', 'page_id' => $faq->id],
            ['name' => 'faq_5_title', 'value' => 'Precio para independientes', 'page_id' => $faq->id],
            ['name' => 'faq_5_body', 'value' => 'Nueva Mirada ecologica', 'page_id' => $faq->id],
            ['name' => 'faq_6_title', 'value' => 'Respaldo de S.F.I.', 'page_id' => $faq->id],
            ['name' => 'faq_6_body', 'value' => 'Nueva Mirada ecologica', 'page_id' => $faq->id],

            ['name' => 'personalidad_main_title', 'value' => 'Personalidad de los arboles contenido de header', 'page_id' => $personalidad->id],
            ['name' => 'personalidad_single_title', 'value' => 'Personalidad de los arboles contenido de mas informacion header', 'page_id' => $personalidad->id],
        ]);
    }
}
