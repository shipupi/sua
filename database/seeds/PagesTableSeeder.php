<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            ['name' => 'home'],
            ['name' => 'servicio'],
            ['name' => 'faq'],
            ['name' => 'personalidad'],
        	['name' => 'contacto'],
        ]);
    }
}
