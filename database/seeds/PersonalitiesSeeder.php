<?php

use Illuminate\Database\Seeder;

class PersonalitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('personalities')->insert([
            ['image' =>'assets/personalidades/1.jpg', 'description' => 'Personalidad 1 descripcion', 'extended' => 'personalidad 1 descripcion extendida','full_left' => 'texto de personalidad lado izquierdo', 'full_right' => 'texto personalidad lado derecho'],
            ['image' =>'assets/personalidades/2.jpg', 'description' => 'Personalidad 2 descripcion', 'extended' => 'personalidad 2 descripcion extendida','full_left' => 'texto de personalidad lado izquierdo', 'full_right' => 'texto personalidad lado derecho'],
            ['image' =>'assets/personalidades/3.jpg', 'description' => 'Personalidad 3 descripcion', 'extended' => 'personalidad 3 descripcion extendida','full_left' => 'texto de personalidad lado izquierdo', 'full_right' => 'texto personalidad lado derecho'],
            ['image' =>'assets/personalidades/4.jpg', 'description' => 'Personalidad 4 descripcion', 'extended' => 'personalidad 4 descripcion extendida','full_left' => 'texto de personalidad lado izquierdo', 'full_right' => 'texto personalidad lado derecho'],
            ['image' =>'assets/personalidades/5.jpg', 'description' => 'Personalidad 5 descripcion', 'extended' => 'personalidad 5 descripcion extendida','full_left' => 'texto de personalidad lado izquierdo', 'full_right' => 'texto personalidad lado derecho'],
            ['image' =>'assets/personalidades/6.jpg', 'description' => 'Personalidad 6 descripcion', 'extended' => 'personalidad 6 descripcion extendida','full_left' => 'texto de personalidad lado izquierdo', 'full_right' => 'texto personalidad lado derecho'],
        ]);
    }
}
