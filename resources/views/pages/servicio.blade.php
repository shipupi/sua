@extends('layouts.master')

@section('content')

<!-- Intro Section -->
    <section class="hero">
        <!-- Hero Slider Section -->
        <div class="flexslider fullscreen-carousel half hero-slider-1 parallax parallax-section1">
            <ul class="slides">
                <li style="background-color: #FFFFFF">
                    <div class="overlay-hero overlay-light">
                        <div class="container caption-hero dark-color">
                            <div class="inner-caption" style="background-image:url('{{ asset($page->getMeta('servicio_header')) }}');")>
                                <!-- <img class="logo" src=" " alt="" draggable="false" /> -->
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <!-- End Hero Slider Section -->
    </section>
    <div class="clearfix"></div>
     <!-- <section style="background-image:url('{{asset($page->getMeta('servicio_header'))}}')" class="inner-intro bg-img24 overlay-dark light-color parallax parallax-background2">
        <div class="inner-caption">
            <h2 class="h2">{!!$page->getMeta('servicio_header_title')!!}</h2>
            <p class="lead">{!!$page->getMeta('servicio_header_subtitle')!!}</p>
        </div>
    </section>
    <div class="clearfix"></div> -->
    <!-- End Intro Section -->
    <!-- Seccion Numerada -->
    <section id="numerada" class="overlay-dark80 light-bg ptb ptb-sm-80">
        <div class="container">
            <div class="row">
                <div class="wow fadeInLeft text-left  pb col-md-12" id="numerada-title">
                    <h1>{!!$page->getMeta('seccion_numerada_title')!!}</h1>
                </div>
            </div>
            <div class="row">
                <div class="wow fadeInDown text-left col-md-4 numerada-col">
                    <h4>{!!$page->getMeta('seccion_numerada_1_title')!!}</h4>
                    <p>{!!$page->getMeta('seccion_numerada_1_body')!!}</p>
                </div>

                <div class="wow fadeInDown text-left col-md-4 numerada-col">
                    <h4>{!!$page->getMeta('seccion_numerada_2_title')!!}</h4>
                    <p>{!!$page->getMeta('seccion_numerada_2_body')!!}</p>
                </div>

                <div class="wow fadeInDown text-left col-md-4 numerada-col">
                    <h4>{!!$page->getMeta('seccion_numerada_3_title')!!}</h4>
                    <p>{!!$page->getMeta('seccion_numerada_3_body')!!}</p>
                </div>
            </div>
        </div>
    </section>
     <!-- End seccion numerada -->
    <section id="youtube-video" class="gray-bg">
        <div class="youtube-container">
            <iframe src="https://www.youtube.com/embed/{{$page->getMeta('servicios_video')}}?controls=0&showinfo=0&enablejsapi=1" frameborder="0" allowfullscreen></iframe>
        </div>
    </section>

    <!-- Slider imagenes -->
   <!--  <section id="fs-slider" class="gray-bg">
        <div class="fs-slider-regulator">
            <div class="fs-slider-extender">
                <div class="fs-slider-container">
                    @foreach(json_decode($page->getMeta('servicio_slider')) as $sliderItem)
                        <img  src="{{asset($sliderItem)}}">
                    @endforeach
                </div>
            </div>
        </div>
    </section> -->
    <!-- Fin Slider imagenes -->
 <!--    Descripcion Servicio -->
    <section id="service" class="service-service gray-bg ptb ptb-sm-80">
        <div class="container">
            <div class="row">
                <div class="desc-servicio wow fadeInDown text-left col-xs-12">
                    <h4>{!!$page->getMeta('servicio_desc_title')!!}</h4>
                    <p>{!!$page->getMeta('servicio_desc_body')!!}</p>
                </div>
            </div>
        </div>
    </section>

   

    <!-- End Descripcion Servicio -->
   
    


    <!-- Seccion Iconos -->
    <section id="iconos" class="gray-bg ptb ptb-sm-80">
        <div class="container">
            <div class="row">
                <div class="wow fadeInLeft text-left col-md-4">
                    <div class="col-xs-2">
                        <i class="icon-planta"></i>
                        <!-- <div class="page-icon-top"><img class="l-black" src="/public/assets/img/logos/p4d_logo.png"/></i></div> -->
                    </div>
                    <div class="col-xs-10">
                        <h5>{!!$page->getMeta('iconos_1_title')!!}</h5>
                        <p>{!!$page->getMeta('iconos_1_body')!!}</p>
                    </div>
                    <div class="clearfix"></div>
                </div>


                <div class="wow fadeInLeft text-left col-md-4">
                    <div class="col-xs-2">
                        <i class="icon-planta"></i>
                        <!-- <div class="page-icon-top"><img class="l-black" src="/public/assets/img/logos/p4d_logo.png"/></i></div> -->
                    </div>
                    <div class="col-xs-10">
                        <h5>{!!$page->getMeta('iconos_2_title')!!}</h5>
                        <p>{!!$page->getMeta('iconos_2_body')!!}</p>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="wow fadeInLeft text-left col-md-4">
                    <div class="col-xs-2">
                        <i class="icon-planta"></i>
                        <!-- <div class="page-icon-top"><img class="l-black" src="/public/assets/img/logos/p4d_logo.png"/></i></div> -->
                    </div>
                    <div class="col-xs-10">
                        <h5>{!!$page->getMeta('iconos_3_title')!!}</h5>
                        <p>{!!$page->getMeta('iconos_3_body')!!}</p>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="wow fadeInLeft text-left col-md-4">
                    <div class="col-xs-2">
                        <i class="icon-planta"></i>
                        <!-- <div class="page-icon-top"><img class="l-black" src="/public/assets/img/logos/p4d_logo.png"/></i></div> -->
                    </div>
                    <div class="col-xs-10">
                        <h5>{!!$page->getMeta('iconos_4_title')!!}</h5>
                        <p>{!!$page->getMeta('iconos_4_body')!!}</p>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="wow fadeInLeft text-left col-md-4">
                    <div class="col-xs-2">
                        <i class="icon-planta"></i>
                        <!-- <div class="page-icon-top"><img class="l-black" src="/public/assets/img/logos/p4d_logo.png"/></i></div> -->
                    </div>
                    <div class="col-xs-10">
                        <h5>{!!$page->getMeta('iconos_5_title')!!}</h5>
                        <p>{!!$page->getMeta('iconos_5_body')!!}</p>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="wow fadeInLeft text-left col-md-4">
                    <div class="col-xs-2">
                        <i class="icon-planta"></i>
                        <!-- <div class="page-icon-top"><img class="l-black" src="/public/assets/img/logos/p4d_logo.png"/></i></div> -->
                    </div>
                    <div class="col-xs-10">
                        <h5>{!!$page->getMeta('iconos_6_title')!!}</h5>
                        <p>{!!$page->getMeta('iconos_6_body')!!}</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </section>

    <!-- End Seccion iconos -->

@endsection

@section('custom_scripts')
<!-- Facebook Pixel Code -->

<script>

!function(f,b,e,v,n,t,s)

{if(f.fbq)return;n=f.fbq=function(){n.callMethod?

n.callMethod.apply(n,arguments):n.queue.push(arguments)};

if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';

n.queue=[];t=b.createElement(e);t.async=!0;

t.src=v;s=b.getElementsByTagName(e)[0];

s.parentNode.insertBefore(t,s)}(window,document,'script',

'https://connect.facebook.net/en_US/fbevents.js');


fbq('init', '709440812594542'); 

fbq('track', 'PageView');

</script>

<noscript>

<img height="1" width="1" 

src="https://www.facebook.com/tr?id=709440812594542&ev=PageView

&noscript=1"/>

</noscript>

<!-- End Facebook Pixel Code -->

<script type="text/javascript">
    
    $(function(){ 
        $('.fs-slider-container').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            arrows: false,
            dots: false,
            swipeToSlide: true
        })

        $('body').on('click', '.fs-slider-container', function() {
            $(this).slick('slickNext')
        })
    })
</script>

@endsection