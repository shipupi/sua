@extends('layouts.master')

@section('content')

    <!-- Intro Section -->

    <!-- Seccion Mapa -->
    <div class="map-container">
        <div id="map" class="map"></div>
    </div>
<!--     <section style="background-image:url('{{asset($page->getMeta('contacto_header'))}}');" class="inner-intro bg-img27 overlay-dark light-color parallax parallax-background2">
    </section> -->
    <div class="clearfix"></div>
    <!-- End Intro Section -->

    <div class="container">
        <h2 class="contact-title">Contactanos</h2>
        <div >Telefono: <a href="tel:08005550037">0800-555-0037</a></div>

        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>

        {!! Form::open(array('route' => 'contact_store', 'class' => 'form')) !!}

        <div class="form-group">
            {!! Form::label('Nombre') !!}
            {!! Form::text('name', null, 
                array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'Nombre')) !!}
        </div>

        <div class="form-group">
            {!! Form::label('E-mail') !!}
            {!! Form::text('email', null, 
                array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'Email')) !!}
        </div>

        <div class="form-group">
            {!! Form::label('Mensaje') !!}
            {!! Form::textarea('message', null, 
                array('required', 
                      'class'=>'form-control', 
                      'placeholder'=>'Mensaje')) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Contactanos', 
              array('class'=>'contactanos-btn btn btn-primary')) !!}
        </div>
        {!! Form::close() !!}
    </div>


@endsection

@section('custom_scripts')
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '709440812594542');
fbq('track', 'PageView');
fbq('track', 'Lead');
</script>
<noscript>
<img height="1" width="1"
src="https://www.facebook.com/tr?id=709440812594542&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
<script type="text/javascript">
    
    $(function(){ 
        $('.fs-slider-container').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            arrows: false,
            dots: false,
            swipeToSlide: true
        })

        $('body').on('click', '.fs-slider-container', function() {
            $(this).slick('slickNext')
        })
    })
</script>

<script>
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 9,
      center: new google.maps.LatLng( -34.5812901, -58.60898085, 13),
      mapTypeId: 'roadmap',
    });
    var myLatLng1 = {lat: -34.6028609,lng:  -58.3830287};
    var myLatLng2 = {lat: -34.5597193, lng: -58.834933};

    var marker = new google.maps.Marker({
      position: myLatLng1,
      map: map,
      title: 'Av. Pres. Roque Sáenz Peña 1160'
    });
    var marker2 = new google.maps.Marker({
      position: myLatLng2,
      map: map,
      title: 'Parque del Buen Retiro'
    });
  }
</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCBS28UQYVFPSdsVhyD2FZnyZ9JlLMxrmU&callback=initMap">
</script>

@endsection