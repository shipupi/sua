@extends('layouts.master')

@section('content')

        <section class="hero">
            <!-- Hero Slider Section -->
            <div class="flexslider fullscreen-carousel hero-slider-1 parallax parallax-section1">
                <ul class="slides">
                    @foreach(json_decode($home->getMeta('slider_top')) as $sliderItem)
                        @if (trim(substr($sliderItem->image, 0,6)) != "assets")
                        <li style="background-color: #FFFFFF">
                            <div class="youtube-container">
                                <iframe src="https://www.youtube.com/embed/{{$sliderItem->image}}?controls=0&showinfo=0&enablejsapi=1" frameborder="0" allowfullscreen></iframe>
                                <!-- <div id="yt-player-{{$loop->index}}" data-link="{{$sliderItem->image}}" class="youtube-embed"></div> -->
                            </div>
                        </li>
                        @else
                        <li style="background-color: #FFFFFF">
                            <div class="overlay-hero overlay-light">
                                <div class="container caption-hero dark-color">
                                    @if (substr($sliderItem->image, -3) == "mp4")
                                        <video loop>
                                            <source src="{{ asset($sliderItem->image)}}" type="video/mp4">
                                        </video>
                                    @else
                                    <div class="inner-caption" style="background-image:url('{{ asset($sliderItem->image) }}');")>
                                        <!-- <img class="logo" src=" " alt="" draggable="false" /> -->
                                        <h2 class="h2">{{$sliderItem->title}}</h2>
                                        <p class="lead">{{$sliderItem->subtitle}}</p>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </li>
                        @endif
                    @endforeach
                </ul>
            </div>
            <!-- End Hero Slider Section -->
        </section>
        <div class="clearfix"></div>
        <!-- End Intro Section -->


        <!-- QUIENES SOMOS -->

        <section id="service" class="gray-bg ptb ptb-sm-80">
            <div class="container">
                <div class="col-md-6 col-xs-12">
                    <h1>{!!$home->getMeta('quienes_somos_main_title')!!}</h1>
                    <p>{!!$home->getMeta('quienes_somos_main_body')!!}</p>
                </div>
                <div class="wow fadeInLeft text-left col-md-6 col-xs-12">
                    <div class="row">
                        <div class="col-xs-2">
                            <i class="icon-sprout"></i>
                            <!-- <div class="page-icon-top"><img class="l-black" src="/public/assets/img/logos/p4d_logo.png"/></i></div> -->
                        </div>
                        <div class="col-xs-10">
                            <h5>{!!$home->getMeta('quienes_somos_1_title')!!}</h5>
                            <p>{!!$home->getMeta('quienes_somos_1_body')!!}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            <i class="icon-sprout"></i>
                        </div>
                        <div class="col-xs-10">
                            <h5>{!!$home->getMeta('quienes_somos_2_title')!!}</h5>
                            <p>{!!$home->getMeta('quienes_somos_2_body')!!}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            <i class="icon-sprout"></i>
                        </div>
                        <div class="col-xs-10">
                            <h5>{!!$home->getMeta('quienes_somos_3_title')!!}</h5>
                            <p>{!!$home->getMeta('quienes_somos_3_body')!!}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Quienes somos -->


        <!-- Counter Section -->
        <section id="counter" class="overlay-dark80 light-color ptb-80" data-stellar-background-ratio="0.5">
            <div class="container text-center">
                <div class="row">
                    <div class="col-md-3 mb-sm-30">
                        <h1 class="counter" data-count="{{$home->getMeta('contadores_1_number')}}">0</h1>
                        <h6>{{$home->getMeta('contadores_1_label')}}</h6>
                    </div>
                    <div class="col-md-3 mb-sm-30">
                        <h1 class="counter" data-count="{{$home->getMeta('contadores_2_number')}}"></h1>
                        <h6>{{$home->getMeta('contadores_2_label')}}</h6>
                    </div>
                    <div class="col-md-3 mb-sm-30">
                        <h1 class="counter" data-count="{{$home->getMeta('contadores_3_number')}}">0</h1>
                        <h6>{{$home->getMeta('contadores_3_label')}}</h6>
                    </div>
                    <div class="col-md-3 mb-sm-30">
                        <h1 class="counter" data-count="{{$home->getMeta('contadores_4_number')}}">0</h1>
                        <h6>{{$home->getMeta('contadores_4_label')}}</h6>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Counter Section -->


        <!-- Process -->
     <!--    <section id="process" class="ptb ptb-sm-80">
            <div class="wow fadeInLeft container text-center">
                <h3>Metodologías ágiles</h3>
                <div class="spacer-60"></div>
                <div class="row">

                    <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="tipped" data-title="Step 1 - Project Plan" data-tipper-options='{"direction":"top"}'>
                            <div class="page-icon-top"><i class="ion ion-ios-paper-outline"></i></div>
                            <h6>Plan</h6>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="tipped" data-title="Step 2 - Development" data-tipper-options='{"direction":"top"}'>
                            <div class="page-icon-top"><i class="ion ion-ios-gear-outline"></i></div>
                            <h6>Development</h6>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="tipped" data-title="Step 3 - Testing" data-tipper-options='{"direction":"top"}'>
                            <div class="page-icon-top"><i class="ion ion-ios-pulse"></i></div>
                            <h6>Testing</h6>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="tipped" data-title="Step 4 - Delivery" data-tipper-options='{"direction":"top"}'>
                            <div class="page-icon-top"><i class="ion ion-ios-clock-outline"></i></div>
                            <h6>Deployment</h6>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->
        <!-- End Process -->

        <!-- Testimonials -->
        <!-- <section id="testimonial" class="overlay-dark80 light-bg ptb ptb-sm-80" data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="owl-carousel testimonial-carousel nf-carousel-theme white">
                    <div class="item">
                        <div class="testimonial text-center light-color">
                            <div class="container-icon"><i class="fa fa-quote-right"></i></div>
                            <p class="lead">" Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sodales lectus eu nisl vulputate, a tempor augue iaculis. Aliquam tempus vulputate ex, eu aliquet dolor imperdiet id. Donec eget sollicitudin elit. Vestibulum at dapibus lorem. Nam volutpat sed eros et bibendum. Praesent non augue vehicula, congue purus in, sagittis est. Integer gravida feugiat efficitur. Proin in justo condimentum, imperdiet arcu et, luctus ex. Integer viverra augue velit, in cursus augue fringilla sed. "</p>
                            <h6 class="quote-author">Socrates</h6>
                            <div class="quote-img"><img src="{{asset('assets/home/founder/profile-circle.png')}}"></div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial text-center light-color">
                            <div class="container-icon"><i class="fa fa-quote-right"></i></div>
                            <p class="lead">" Mauris dignissim odio tortor, ut iaculis ex tincidunt ac. Nunc vel sapien sit amet est euismod suscipit. Fusce lobortis est lorem, eu tincidunt erat malesuada eu. Nullam sed rhoncus nunc. Sed ac augue tellus. Integer nulla risus, hendrerit vitae aliquet rhoncus, posuere quis risus. Nulla ornare dapibus tortor vitae hendrerit. Donec ullamcorper mauris vitae ultricies vehicula. Mauris eget eros id odio iaculis mollis. Maecenas rhoncus velit velit, non laoreet lectus luctus a. Morbi eget urna aliquam ligula interdum laoreet in a nisi. Sed dapibus ante eget purus lobortis, vel convallis turpis tristique. "</p>
                            <h6 class="quote-author">Julius Augustus</h6>
                            <div class="quote-img"><img src="{{asset('assets/home/founder/profile-circle2.png')}}"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->
        <!-- End Testimonials -->


        <!-- Link Contacto -->

        <section id="contacto-btn" class="ptb ptb-sm-80">
            <div class="container">
                <div class="contacto-btn-title">
                    CONTACTANOS
                </div>
                <div class="contacto-btn-btn-wrapper">
                    <a href="{{url('contacto')}}">
                        <div class="contacto-btn-btn">
                            Click Aca
                        </div>
                    </a>
                </div>
            </div>
        </section>        

        <!-- Blog Section -->
        <!-- <section id="blog" class="wow fadeIn ptb ptb-sm-80 bg-img4">
            <div class="container">
                <h3 class="float-left float-none-xs">Latest Blog</h3>
                <a class="btn-link-a float-right float-none-xs">View All</a>
                <div class="clearfix"></div>
                <div class="spacer-60"></div>
                <div class="row">

                    <div class="col-lg-4 col-md-6 col-sm-6 mb-sm-30">
                        <div class="blog-post">
                            <div class="post-media">
                                <video class="video" style="width: 100%;" width="" height="" preload="auto" poster="{{ asset('assets/media/echo-hereweare.jpg') }}" controls="controls">
                                    <source type="video/mp4" src="{{ asset('assets/media/video1.mp4') }}" />
                                    <source type="video/webm" src="{{ asset('assets/media/video1.webm') }}" />
                                    <source type="video/ogg" src="{{ asset('assets/media/video1.ogv') }}" />
                                </video>
                            </div>
                            <div class="post-meta"><span>by <a>John Doe</a>,</span> <span>Mar 16, 2016</span></div>
                            <div class="post-header">
                                <h5><a href="">Maecenas nec odio ante varcy</a></h5>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-6 mb-sm-30">
                        <div class="blog-post">
                            <div class="post-media">
                                <div class="owl-carousel item1-carousel nf-carousel-theme">
                                    <div class="item">
                                        <img src="{{ asset('assets/img/full/26.jpg') }}" alt="" />
                                    </div>
                                    <div class="item">
                                        <img src="{{ asset('assets/img/full/04.jpg') }}" alt="" />
                                    </div>
                                    <div class="item">
                                        <img src="{{ asset('assets/img/full/27.jpg') }}" alt="" />
                                    </div>
                                </div>
                            </div>
                            <div class="post-meta"><span>by <a>John Doe</a>,</span> <span>Feb 24, 2016</span></div>
                            <div class="post-header">
                                <h5><a href="">onec pede justo, fringilla vel</a></h5>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-6 mb-sm-30">
                        <div class="blog-post">
                            <div class="post-media">
                                <img class="item-container" src="{{ asset('assets/img/full/20.jpg') }}" alt="" />
                            </div>
                            <div class="post-meta"><span>by <a>John Doe</a>,</span> <span>Jan 29, 2016</span></div>
                            <div class="post-header">
                                <h5><a href="">Augue velit cursus nunc</a></h5>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section> -->
        <!-- End Blog Section -->

        <!-- Client Logos Section -->
        <!-- <section id="client-logos" class="wow fadeIn ptb ptb-sm-80">
            <div class="container">
                <div class="owl-carousel client-carousel nf-carousel-theme ">
                    <div class="item">
                        <div class="client-logo">
                            <img src="{{ asset('assets/img/logos/1.png') }}" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </section> -->
        <!-- End Client Logos Section -->

@endsection


@section('custom_scripts')
<!-- Facebook Pixel Code -->

<script>

!function(f,b,e,v,n,t,s)

{if(f.fbq)return;n=f.fbq=function(){n.callMethod?

n.callMethod.apply(n,arguments):n.queue.push(arguments)};

if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';

n.queue=[];t=b.createElement(e);t.async=!0;

t.src=v;s=b.getElementsByTagName(e)[0];

s.parentNode.insertBefore(t,s)}(window,document,'script',

'https://connect.facebook.net/en_US/fbevents.js');


fbq('init', '709440812594542'); 

fbq('track', 'PageView');

</script>

<noscript>

<img height="1" width="1" 

src="https://www.facebook.com/tr?id=709440812594542&ev=PageView

&noscript=1"/>

</noscript>

<!-- End Facebook Pixel Code -->
<script type="text/javascript">
    
    $(function(){ 
        var infinitePreventer = 0
        var loadedInterval = setInterval(function() {
            if (typeof createYoutube != "function") {
               console.log('not loaded yet') 
               infinitePreventer += 1
               if (infinitePreventer > 25) {
                    clearInterval(loadedInterval)
                    console.log('aborted too many attempts')
               }
            } else {
                console.log( 'created youtube function')
                clearInterval(loadedInterval)


                $('.flexslider iframe').each(function(){
                    // Create a new player pointer; "this" is a DOMElement of the player's iframe
                    var player = new YT.Player(this, {
                        playerVars: {
                            autoplay: 0
                        }
                    });

                    player.addEventListener("onStateChange", function(state){
                        switch(state.data)
                        {
                            // If the user is playing a video, stop the slider
                            case YT.PlayerState.PLAYING:
                                homeSlider.flexslider("stop");
                                canSlide = false;
                                break;
                            // The video is no longer player, give the go-ahead to start the slider back up
                            case YT.PlayerState.ENDED:
                            case YT.PlayerState.PAUSED:
                                homeSlider.flexslider("play");
                                canSlide = true;
                                break;
                        }
                    });
         
                    // Watch for changes on the player

                    $(this).data('player', player);
                });

                // $('.youtube-container iframe').each(function(index) {
                //     var $this = $(this)
                //     var id = $this.attr('id')
                //     console.log(id)
                //     var link = $this.data('link')
                //     var ytObj = createYoutube(id, link)
                //     youtubePlayers.push(ytObj)
                //     // youtubePlayers.push('')
                // })
            }
        }, 200);
    });

</script>
@endsection