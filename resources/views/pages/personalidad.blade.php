@extends('layouts.master')

@section('content')

    <!-- Intro Section -->

    <section class="hero">
            <!-- Hero Slider Section -->
            <div class="flexslider fullscreen-carousel half hero-slider-1 parallax parallax-section1">
                <ul class="slides">
                    <li style="background-color: #FFFFFF">
                        <div class="overlay-hero overlay-light">
                            <div class="container caption-hero dark-color">
                                <div class="inner-caption" style="background-image:url('{{ asset($page->getMeta('personalidad_header')) }}');")>
                                    <!-- <img class="logo" src=" " alt="" draggable="false" /> -->
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- End Hero Slider Section -->
        </section>
        <div class="clearfix"></div>
  <!--   <section style="background-image:url('{{asset($page->getMeta('personalidad_header'))}}')" class="inner-intro bg-img24 overlay-dark light-color parallax parallax-background2">
    </section>
    <div class="clearfix"></div> -->
    <!-- End Intro Section -->


    <!--    Descripcion Servicio -->

    <section id="service" class="gray-bg ptb ptb-sm-80">
        <div class="container">
            <div class="row">
                <div class="wow fadeInDown text-center col-xs-12">
                    <p>{!!$page->getMeta('personalidad_main_title')!!}</p>
                </div>
            </div>
        </div>
    </section>
    <!-- End Descripcion Servicio -->

    <!-- Seccion Iconos -->
    <section id="lista-personalidades" class="overlay-dark80 light-bg ptb ptb-sm-80">
        <div class="container">
            <div class="row">
                @foreach($personalities as $personality)
                <div class="col-md-4 col-xs-12 personality-container" data-extended="{{$personality->extended}}" data-href="{{url('personalidad/' . $personality->id)}}" data-id="{{$personality->id}}">
                    <div class="personality-wrapper wow fadeInLeft text-left">
                        <div class="personality-image">
                            <img src="{{asset($personality->image)}}">
                        </div>
                        <div class=" personality-description">
                            {!!$personality->description!!}
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>

    <!-- End Seccion iconos -->
    <div class="extended-personality-anchor"></div>
    <section id="extended-personality" class="gray-bg ptb ptb-sm-80">
        <div id="extended-text" class="text-center col-xs-12">
        </div>
        <div class="contacto-btn-btn-wrapper">
            <a href="#">
                <div class="leer-mas-personalidad">
                    Leer más
                </div>
            </a>
        </div>
    </section>


@endsection

@section('custom_scripts')
<!-- Facebook Pixel Code -->

<script>

!function(f,b,e,v,n,t,s)

{if(f.fbq)return;n=f.fbq=function(){n.callMethod?

n.callMethod.apply(n,arguments):n.queue.push(arguments)};

if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';

n.queue=[];t=b.createElement(e);t.async=!0;

t.src=v;s=b.getElementsByTagName(e)[0];

s.parentNode.insertBefore(t,s)}(window,document,'script',

'https://connect.facebook.net/en_US/fbevents.js');


fbq('init', '709440812594542'); 

fbq('track', 'PageView');

</script>

<noscript>

<img height="1" width="1" 

src="https://www.facebook.com/tr?id=709440812594542&ev=PageView

&noscript=1"/>

</noscript>

<!-- End Facebook Pixel Code -->
<script type="text/javascript">
    $(function(){
        $.fn.extend({
            animateCss: function (animationName) {
                var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
                this.addClass('animated ' + animationName).one(animationEnd, function() {
                    $(this).removeClass('animated ' + animationName);
                });
            }
        });

        var $selected = null

        $('body').on('click', '.personality-wrapper', function() {
            $selected = $(this)
            var $container = $selected.closest('.personality-container')
            var data = $container.data('extended')
            $('#extended-text').html(data)
            $('#extended-personality').show()
            $('#extended-personality').animateCss('fadeInDown')
            $('#extended-personality a').attr('href', $container.data('href'))
            setTimeout(function() {
                scrollToAnchor();
            }, 250);
        })



        function scrollToAnchor(){
            var aTag2 = $(".extended-personality-anchor");
            var halfway = aTag2.offset().top - ($(window).height() / 4)
            $('html,body').animate({scrollTop: halfway},'slow');
        }

        
    })
</script>

@endsection