@extends('layouts.master')

@section('content')

<!-- Intro Section -->
    <section class="hero">
        <!-- Hero Slider Section -->
        <div class="flexslider fullscreen-carousel half hero-slider-1 parallax parallax-section1">
            <ul class="slides">
                <li style="background-color: #FFFFFF">
                    <div class="overlay-hero overlay-light">
                        <div class="container caption-hero dark-color">
                            <div class="inner-caption" style="background-image:url('{{ asset($page->getMeta('faq_header')) }}');")>
                                <!-- <img class="logo" src=" " alt="" draggable="false" /> -->
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <!-- End Hero Slider Section -->
    </section>
    <div class="clearfix"></div>
    <!--  <section style="background-image:url('{{asset($page->getMeta('faq_header'))}}')" class="inner-intro bg-img26 overlay-dark light-color parallax parallax-background2">
    </section>
    <div class="clearfix"></div> -->
    <!-- End Intro Section -->

    <!-- Seccion Iconos -->
    <section id="iconos" class="ptb ptb-sm-80">
        <div class="container">
            <div class="row">
                <div class="faq-row row">
                    <div class="wow fadeInLeft text-left col-xs-12 col-md-4">
                        <div class="faq-box">
                            <div class="col-xs-2">
                                <i class="icono-arbol"></i>
                                <!-- <div class="page-icon-top"><img class="l-black" src="/public/assets/img/logos/p4d_logo.png"/></i></div> -->
                            </div>
                            <div class="col-xs-10">
                                <h5>{!!$page->getMeta('faq_1_title')!!}</h5>
                                <p>{!!$page->getMeta('faq_1_body')!!}</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>


                    <div class="wow fadeInLeft text-left col-xs-12 col-md-4">
                        <div class="faq-box">
                            <div class="col-xs-2">
                                <i class="icono-arbol"></i>
                                <!-- <div class="page-icon-top"><img class="l-black" src="/public/assets/img/logos/p4d_logo.png"/></i></div> -->
                            </div>
                            <div class="col-xs-10">
                                <h5>{!!$page->getMeta('faq_2_title')!!}</h5>
                                <p>{!!$page->getMeta('faq_2_body')!!}</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="wow fadeInLeft text-left col-xs-12 col-md-4">
                        <div class="faq-box">
                            <div class="col-xs-2">
                                <i class="icono-arbol"></i>
                                <!-- <div class="page-icon-top"><img class="l-black" src="/public/assets/img/logos/p4d_logo.png"/></i></div> -->
                            </div>
                            <div class="col-xs-10">
                                <h5>{!!$page->getMeta('faq_3_title')!!}</h5>
                                <p>{!!$page->getMeta('faq_3_body')!!}</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="faq-row row">
                    <div class="wow fadeInLeft text-left col-xs-12 col-md-4">
                        <div class="faq-box">
                            <div class="col-xs-2">
                                <i class="icono-arbol"></i>
                                <!-- <div class="page-icon-top"><img class="l-black" src="/public/assets/img/logos/p4d_logo.png"/></i></div> -->
                            </div>
                            <div class="col-xs-10">
                                <h5>{!!$page->getMeta('faq_4_title')!!}</h5>
                                <p>{!!$page->getMeta('faq_4_body')!!}</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="wow fadeInLeft text-left col-xs-12 col-md-4">
                        <div class="faq-box">
                            <div class="col-xs-2">
                                <i class="icono-arbol"></i>
                                <!-- <div class="page-icon-top"><img class="l-black" src="/public/assets/img/logos/p4d_logo.png"/></i></div> -->
                            </div>
                            <div class="col-xs-10">
                                <h5>{!!$page->getMeta('faq_5_title')!!}</h5>
                                <p>{!!$page->getMeta('faq_5_body')!!}</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="wow fadeInLeft text-left col-xs-12 col-md-4">
                        <div class="faq-box">
                            <div class="col-xs-2">
                                <i class="icono-arbol"></i>
                                <!-- <div class="page-icon-top"><img class="l-black" src="/public/assets/img/logos/p4d_logo.png"/></i></div> -->
                            </div>
                            <div class="col-xs-10">
                                <h5>{!!$page->getMeta('faq_6_title')!!}</h5>
                                <p>{!!$page->getMeta('faq_6_body')!!}</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>


                <div class="faq-row row">
                    <div class="wow fadeInLeft text-left col-xs-12 col-md-4">
                        <div class="faq-box">
                            <div class="col-xs-2">
                                <i class="icono-arbol"></i>
                                <!-- <div class="page-icon-top"><img class="l-black" src="/public/assets/img/logos/p4d_logo.png"/></i></div> -->
                            </div>
                            <div class="col-xs-10">
                                <h5>{!!$page->getMeta('faq_7_title')!!}</h5>
                                <p>{!!$page->getMeta('faq_7_body')!!}</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>


                    <div class="wow fadeInLeft text-left col-xs-12 col-md-4">
                        <div class="faq-box">
                            <div class="col-xs-2">
                                <i class="icono-arbol"></i>
                                <!-- <div class="page-icon-top"><img class="l-black" src="/public/assets/img/logos/p4d_logo.png"/></i></div> -->
                            </div>
                            <div class="col-xs-10">
                                <h5>{!!$page->getMeta('faq_8_title')!!}</h5>
                                <p>{!!$page->getMeta('faq_8_body')!!}</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="wow fadeInLeft text-left col-xs-12 col-md-4">
                        <div class="faq-box">
                            <div class="col-xs-2">
                                <i class="icono-arbol"></i>
                                <!-- <div class="page-icon-top"><img class="l-black" src="/public/assets/img/logos/p4d_logo.png"/></i></div> -->
                            </div>
                            <div class="col-xs-10">
                                <h5>{!!$page->getMeta('faq_9_title')!!}</h5>
                                <p>{!!$page->getMeta('faq_9_body')!!}</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="faq-row row">
                    <div class="wow fadeInLeft text-left col-xs-12 col-md-4">
                        <div class="faq-box">
                            <div class="col-xs-2">
                                <i class="icono-arbol"></i>
                                <!-- <div class="page-icon-top"><img class="l-black" src="/public/assets/img/logos/p4d_logo.png"/></i></div> -->
                            </div>
                            <div class="col-xs-10">
                                <h5>{!!$page->getMeta('faq_10_title')!!}</h5>
                                <p>{!!$page->getMeta('faq_10_body')!!}</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="wow fadeInLeft text-left col-xs-12 col-md-4">
                        <div class="faq-box">
                            <div class="col-xs-2">
                                <i class="icono-arbol"></i>
                                <!-- <div class="page-icon-top"><img class="l-black" src="/public/assets/img/logos/p4d_logo.png"/></i></div> -->
                            </div>
                            <div class="col-xs-10">
                                <h5>{!!$page->getMeta('faq_11_title')!!}</h5>
                                <p>{!!$page->getMeta('faq_11_body')!!}</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="wow fadeInLeft text-left col-xs-12 col-md-4">
                        <div class="faq-box">
                            <div class="col-xs-2">
                                <i class="icono-arbol"></i>
                                <!-- <div class="page-icon-top"><img class="l-black" src="/public/assets/img/logos/p4d_logo.png"/></i></div> -->
                            </div>
                            <div class="col-xs-10">
                                <h5>{!!$page->getMeta('faq_12_title')!!}</h5>
                                <p>{!!$page->getMeta('faq_12_body')!!}</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>


                <div class="faq-row row">
                    <div class="wow fadeInLeft text-left col-xs-12 col-md-4">
                        <div class="faq-box">
                            <div class="col-xs-2">
                                <i class="icono-arbol"></i>
                                <!-- <div class="page-icon-top"><img class="l-black" src="/public/assets/img/logos/p4d_logo.png"/></i></div> -->
                            </div>
                            <div class="col-xs-10">
                                <h5>{!!$page->getMeta('faq_13_title')!!}</h5>
                                <p>{!!$page->getMeta('faq_13_body')!!}</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>


                    <div class="wow fadeInLeft text-left col-xs-12 col-md-4">
                        <div class="faq-box">
                            <div class="col-xs-2">
                                <i class="icono-arbol"></i>
                                <!-- <div class="page-icon-top"><img class="l-black" src="/public/assets/img/logos/p4d_logo.png"/></i></div> -->
                            </div>
                            <div class="col-xs-10">
                                <h5>{!!$page->getMeta('faq_14_title')!!}</h5>
                                <p>{!!$page->getMeta('faq_14_body')!!}</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="wow fadeInLeft text-left col-xs-12 col-md-4">
                        <div class="faq-box">
                            <div class="col-xs-2">
                                <i class="icono-arbol"></i>
                                <!-- <div class="page-icon-top"><img class="l-black" src="/public/assets/img/logos/p4d_logo.png"/></i></div> -->
                            </div>
                            <div class="col-xs-10">
                                <h5>{!!$page->getMeta('faq_15_title')!!}</h5>
                                <p>{!!$page->getMeta('faq_15_body')!!}</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="faq-row row">
                    <div class="wow fadeInLeft text-left col-xs-12 col-md-4">
                        <div class="faq-box">
                            <div class="col-xs-2">
                                <i class="icono-arbol"></i>
                                <!-- <div class="page-icon-top"><img class="l-black" src="/public/assets/img/logos/p4d_logo.png"/></i></div> -->
                            </div>
                            <div class="col-xs-10">
                                <h5>{!!$page->getMeta('faq_16_title')!!}</h5>
                                <p>{!!$page->getMeta('faq_16_body')!!}</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="wow fadeInLeft text-left col-xs-12 col-md-4">
                        <div class="faq-box">
                            <div class="col-xs-2">
                                <i class="icono-arbol"></i>
                                <!-- <div class="page-icon-top"><img class="l-black" src="/public/assets/img/logos/p4d_logo.png"/></i></div> -->
                            </div>
                            <div class="col-xs-10">
                                <h5>{!!$page->getMeta('faq_17_title')!!}</h5>
                                <p>{!!$page->getMeta('faq_17_body')!!}</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="wow fadeInLeft text-left col-xs-12 col-md-4">
                        <div class="faq-box">
                            <div class="col-xs-2">
                                <i class="icono-arbol"></i>
                                <!-- <div class="page-icon-top"><img class="l-black" src="/public/assets/img/logos/p4d_logo.png"/></i></div> -->
                            </div>
                            <div class="col-xs-10">
                                <h5>{!!$page->getMeta('faq_18_title')!!}</h5>
                                <p>{!!$page->getMeta('faq_18_body')!!}</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </section>

    <!-- End Seccion iconos -->

@endsection

@section('custom_scripts')
<!-- Facebook Pixel Code -->

<script>

!function(f,b,e,v,n,t,s)

{if(f.fbq)return;n=f.fbq=function(){n.callMethod?

n.callMethod.apply(n,arguments):n.queue.push(arguments)};

if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';

n.queue=[];t=b.createElement(e);t.async=!0;

t.src=v;s=b.getElementsByTagName(e)[0];

s.parentNode.insertBefore(t,s)}(window,document,'script',

'https://connect.facebook.net/en_US/fbevents.js');


fbq('init', '709440812594542'); 

fbq('track', 'PageView');

</script>

<noscript>

<img height="1" width="1" 

src="https://www.facebook.com/tr?id=709440812594542&ev=PageView

&noscript=1"/>

</noscript>

<!-- End Facebook Pixel Code -->
<script type="text/javascript">
    
    $(function(){ 
        var adjustBoxesHeight = function() {
            // var maxHeight = 1
            console.log('windowWW')
            console.log($(window).width())
            if ($(window).width() > 1024) {
                $('.faq-box').each(function(index,value) {
                    var parentH = $(this).closest('.faq-row').outerHeight()
                    console.log(parentH)
                    $(this).outerHeight(parentH - 20)
                    // var height = $(this).outerHeight()
                    // if (height > maxHeight) {
                    //     maxHeight = height
                    // }
                })
            } else {
                $('.faq-box').each(function(index,value) {
                    $(this).height('100%')
                });
            }

            // $('.faq-box').css('height', maxHeight + 'px')
        }

        adjustBoxesHeight()
        $(window).resize(function() {
            adjustBoxesHeight()
        })
    })
</script>

@endsection