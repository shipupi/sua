<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Seamos un arbol</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="nileforest">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

     <!-- Favicone Icon  -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- CSS -->
    <link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/font-awesome.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/ionicons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/plugin/jPushMenu.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/plugin/animate.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/plugin/slick.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/jquery-ui.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css" />

    <!-- Facebook Pixel Code -->
<script>

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){

(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),

m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)

})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-71839615-1', 'auto');

ga('send', 'pageview');

</script>


<!-- Google ad code -->
<script>

!function(f,b,e,v,n,t,s)

{if(f.fbq)return;n=f.fbq=function(){n.callMethod?

n.callMethod.apply(n,arguments):n.queue.push(arguments)};

if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';

n.queue=[];t=b.createElement(e);t.async=!0;

t.src=v;s=b.getElementsByTagName(e)[0];

s.parentNode.insertBefore(t,s)}(window,document,'script',

'https://connect.facebook.net/en_US/fbevents.js');


fbq('init', '709440812594542'); 

fbq('track', 'PageView');

</script>

<noscript>

<img height="1" width="1" 

src="https://www.facebook.com/tr?id=709440812594542&ev=PageView

&noscript=1"/>

</noscript>

<!-- End Facebook Pixel Code -->
    <!-- youtube api -->
</head>
<body class="full-intro always-sticky sticky">

    <!-- Preloader -->
    <section id="preloader">
        <div class="loader" id="loader">
            <div class="loader-img"></div>
        </div>
    </section>
    <!-- End Preloader -->



    <!-- Search menu Top -->
    <section class=" top-search-bar cbp-spmenu cbp-spmenu-horizontal cbp-spmenu-top">
        <div class="container">
            <div class="search-wraper">
                <input type="text" class="input-sm form-full" placeholder="Search..." name="search" />
                <a class="search-bar-icon"><i class="fa fa-search"></i></a>
                <a class="bar-close toggle-menu menu-top push-body"><i class="ion ion-android-close"></i></a>
            </div>
        </div>
    </section>
    <!--End Search menu Top -->

    <!-- Site Wraper -->
    <div class="wrapper">

        <!-- HEADER -->
        <header class="header sticky">
            <div class="container-fluid">

                <!-- logo -->
                <!--End logo-->

                <!-- Rightside Menu (Search, Cart, Bart icon) -->
                <div class="side-menu-btn">
                    <ul>
                        <!-- Search Icon -->
                        <li class="">
                            <a class="right-icon menu-top" href="tel:08005550037">0800-555-0037</a>
                        </li>
                        <!-- End Search Icon -->
                    </ul>
                </div>
                <!-- End Rightside Menu -->

                <!-- Navigation Menu -->
                    <div class="logo-container">
                        <img src="{{asset('assets/master/logo-color.png')}}">
                    </div>
                    <nav class='navigation navbar-fixed-top'>
                    <ul>
                        <li>
                            <a href="{{url('home')}}">HOME</a>
                        </li>

                        <li>
                            <a href="{{url('servicio')}}">NUESTRO SERVICIO</a>
                        </li>
                        <li>
                            <a href="{{url('personalidad')}}">ESPECIES QUE PLANTAMOS</a>
                        </li>
                        <li>
                            <a href="{{url('preguntas_frecuentes')}}">PREGUNTAS FRECUENTES</a>
                        </li>
                        <li>
                            <a href="{{url('contacto')}}">CONTACTO</a>
                        </li>

                    </ul>
                </nav>
                <!--End Navigation Menu -->

            </div>
        </header>
        <!-- END HEADER -->
        <!-- CONTENT-->
         <!-- Intro Section -->
    <section style="padding-top: 70px;" class="hero">
        <!-- Hero Slider Section -->
        <div class="flexslider fullscreen-carousel half allowed-navigation hero-slider-1 parallax parallax-section1">
            <ul class="slides">
                @foreach(json_decode($personality->slider_images) as $sliderItem)
                <li style="background-color: #FFFFFF">
                    <div class="overlay-hero overlay-light">
                        <div class="container caption-hero dark-color">
                            <div class="inner-caption" style="background-image:url('{{ asset($sliderItem) }}');")>
                                <!-- <img class="logo" src=" " alt="" draggable="false" /> -->
                            </div>
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
        <!-- End Hero Slider Section -->
    </section>
    <div class="clearfix"></div>
    <!-- <section class="inner-intro bg-img24 overlay-dark light-color parallax parallax-background2" style="background-image:url('{{asset($personality->header)}}');">
    </section> -->
    <div class="clearfix"></div>
    <!-- End Intro Section -->

    <!-- Slider imagenes -->
<!--     <section id="fs-slider" class="gray-bg">
        <div class="fs-slider-regulator">
            <div class="fs-slider-extender">
                <div class="fs-slider-container">
                    @foreach(json_decode($personality->slider_images) as $sliderItem)
                        <img  src="{{asset($sliderItem)}}">
                    @endforeach
                </div>
            </div>
        </div>
    </section> -->
    <!-- Fin Slider imagenes -->


    <!-- Seccion Iconos -->
    <section class="gray-bg ptb ptb-sm-80">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-12 wow fadeInLeft text-left">
                    {!!$personality->full_left!!}
                </div>
                <div class="col-md-6 col-xs-12 wow fadeInRight text-left">
                    {!!$personality->full_right!!}
                </div>
            </div>
        </div>
    </section>
    <!--    Descripcion Servicio -->

    <section id="descripcion-personalidad" class="dark-bg ptb ptb-sm-80">
        <div class="container">
            <div class="row">
                <div class="wow fadeInDown text-center col-xs-12">
                    <p>{!!$page->getMeta('personalidad_single_title')!!}</p>
                </div>
            </div>
        </div>
    </section>
    <!-- End Descripcion Servicio -->
        <!-- Intro Section -->

        <!-- END CONTENT -->

        <!-- FOOTER -->
               <footer class="footer pt-80">
            <div class="container">
                <div class="row mb-60">
                    <!-- Logo -->
                    <div class="col-md-3 col-sm-3 col-xs-12 mb-xs-30">
                        <a class="footer-logo" href="/">
                            <img src="{{asset('assets/master/logo-grises.png')}}" /></a>
                    </div>
                    <!-- Logo -->

                    <!-- Newsletter -->
                    <!-- <div class="col-md-4 col-sm-5 col-xs-12 mb-xs-30">
                        <div class="newsletter">
                            <form>
                                <input type="email" class="newsletter-input input-md newsletter-input mb-0" placeholder="Enter Your Email">
                                <button class="newsletter-btn btn btn-xs btn-white" type="submit" value=""><i class="fa fa-angle-right mr-0"></i></button>
                            </form>
                        </div>
                    </div> -->
                    <!-- End Newsletter -->

                    <!-- Social -->
                    <div class="col-md-3 col-md-offset-6 col-sm-offset-5 col-sm-4 col-xs-12">
                        <div class="footer-contact direccion">R.P. 24 6164, Moreno, Buenos Aires</div>
                        <div class="footer-contact direccion">Diagonal Norte 1160, 5A, CABA.</div>
                        <div class="footer-contact telefono"><a href="tel:08005550037">0800-555-0037</a></div>
                    </div>
                    
                    <!-- End Social -->
                </div>
                <!--Footer Info -->
                <!-- End Footer Info -->
            </div>

            <hr />

            <!-- Copyright Bar -->
            <section class="copyright ptb-60">
                <div class="container">
                    <div class="col-md-6 col-sm-4 col-xs-12">
                        © {{date('Y')}} <a><b>Seamos un arbol</b></a>. Todos los derechos reservados.
                    </div>
                    <div class="col-md-3 col-md-offset-2 col-sm-4 col-xs-12">
                        <ul class="social">
                           <li><a target="_blank" href="https://www.facebook.com/Seamosunarbol"><i class="fa fa-facebook"></i></a></li>
                            <li><a target="_blank" href="https://www.instagram.com/seamosunarbol/"><i class="fa fa-instagram"></i></a></li>
                            <li><a target="_blank" href="https://www.youtube.com/channel/UCioGndIJ3xr0KFFOaTf52yA"><i class="fa fa-youtube"></i></a></li>
                        </ul>
                    </div>
                </div>
            </section>
            <!-- End Copyright Bar -->

        </footer>
        <!-- END FOOTER -->

        <!-- Scroll Top -->
        <a class="scroll-top">
            <i class="fa fa-angle-double-up"></i>
        </a>
        <!-- End Scroll Top -->

    </div>
    <!-- Site Wraper End -->


    <!-- JS -->

    <script type="text/javascript" src="{{ asset('assets/js/jquery-1.11.2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/jquery.flexslider.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/background-check.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/jquery.fitvids.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/jquery.viewportchecker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/jquery.stellar.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/wow.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/jquery.colorbox-min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/isotope.pkgd.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/masonry.pkgd.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/slick.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/imagesloaded.pkgd.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/jPushMenu.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/jquery.fs.tipper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/mediaelement-and-player.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/theme.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/navigation.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/app.js') }}"></script>

    <!-- CONTENT-->
<script type="text/javascript">
    $(function(){
        $('.fs-slider-container').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            arrows: false,
            dots: false,
            swipeToSlide: true
        })

        $('body').on('click', '.fs-slider-container', function() {
            $(this).slick('slickNext')
        })
    })
</script>
</body>
</html>
