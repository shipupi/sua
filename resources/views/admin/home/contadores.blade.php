@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection
@section('contentheader_title')
	Contadores
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border text-center">
						<h3 class="box-title">Home</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
				{!! Form::open(['url' => 'admin/home/update', 'files' => true]) !!}
				{!! Form::hidden('page_id' , $page->id) !!}
				{!! Form::hidden('redirect' , 'admin/home/contadores') !!}

				<!-- Quienes Somos Main Izquierda -->
				<div class="form-group">
					{!! Form::label('contadores_1_label', 'Contadores 1 texto') !!}
					{!! Form::text('contadores_1_label', $page->getMeta('contadores_1_label')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('contadores_1_number', 'Contadores 1 numero') !!}
					{!! Form::number('contadores_1_number', $page->getMeta('contadores_1_number')) !!}
				</div>


				<div class="form-group">
					{!! Form::label('contadores_2_label', 'Contadores 2 texto') !!}
					{!! Form::text('contadores_2_label', $page->getMeta('contadores_2_label')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('contadores_2_number', 'Contadores 2 numero') !!}
					{!! Form::number('contadores_2_number', $page->getMeta('contadores_2_number')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('contadores_3_label', 'Contadores 3 texto') !!}
					{!! Form::text('contadores_3_label', $page->getMeta('contadores_3_label')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('contadores_3_number', 'Contadores 3 numero') !!}
					{!! Form::number('contadores_3_number', $page->getMeta('contadores_3_number')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('contadores_4_label', 'Contadores 4 texto') !!}
					{!! Form::text('contadores_4_label', $page->getMeta('contadores_4_label')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('contadores_4_number', 'Contadores 4 numero') !!}
					{!! Form::number('contadores_4_number', $page->getMeta('contadores_4_number')) !!}
				</div>

				<div class="form-group">
					{!! Form::submit('Enviar') !!}
				</div>
				{!! Form::close() !!}

			</div>
		</div>
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
</script>

@endsection