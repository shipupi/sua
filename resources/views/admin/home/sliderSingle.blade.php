@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection
@section('contentheader_title')
	Quienes Somos - Item 1
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border text-center">
						<h3 class="box-title">Home</h3>


						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
				{!! Form::open(['url' => 'admin/home/updateSingleSlider/' . $id, 'files' => true]) !!}
				{!! Form::hidden('page_id' , $page->id) !!}
				{!! Form::hidden('redirect' , 'admin/home/slider') !!}

				<div class="form-group row">
						<div class="col-md-3">{!! Form::label('titulo', 'Titulo') !!}</div>
						<div class="col-md-3">{!! Form::text('titulo', $slider[$id]->title) !!}</div>
				</div>

				<div class="form-group row">
					<div class="col-md-3">{!! Form::label('subtitle', 'Subtitulo') !!}</div>
					<div class="col-md-3">{!! Form::text('subtitle', $slider[$id]->subtitle) !!}</div>
				</div>

				<div class="row">
					
					<div class="col-md-5 col-md-offset-1" >
						<img style="width:100%;" src="{{asset($slider[$id]->image)}}">
					</div>

				</div>
				<div class="form-group">
				    {!! Form::label('Subir nueva imagen') !!}
				    {!! Form::file('image', null) !!}
				</div>
				<div class="form-group">
					{!! Form::submit('Enviar') !!}
				</div>
				{!! Form::close() !!}

			</div>
		</div>
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
	$(function(){ 
		CKEDITOR.replace( 'quienes_somos_1_body' );
	})
</script>

@endsection