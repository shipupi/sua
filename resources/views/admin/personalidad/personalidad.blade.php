@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection
@section('contentheader_title')
	Personalidad
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border text-center">
						<h3 class="box-title">Personalidad {{$personality->id}}</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
				{!! Form::open(['url' => 'admin/personalidad/updatePersonality', 'files' => true]) !!}
				{!! Form::hidden('personality_id' , $personality->id) !!}
				{!! Form::hidden('redirect' , 'admin/personalidad/personalidades') !!}

				<!-- Quienes Somos Main Izquierda -->
				<div class="form-group">
					{!! Form::label('description', 'Descripcion') !!}
					{!! Form::textarea('description', $personality->description) !!}
				</div>

				<div class="form-group">
					{!! Form::label('extended', 'Descripcion Extendida (Se aparece al clickear la descripcion)') !!}
					{!! Form::textarea('extended', $personality->extended) !!}
				</div>

				<div class="form-group">
					{!! Form::label('full_left', 'Columna Izquierda de la descripción completa') !!}
					{!! Form::textarea('full_left', $personality->full_left) !!}
				</div>

				<div class="form-group">
					{!! Form::label('full_right', 'Columna Derecha de la descripción completa') !!}
					{!! Form::textarea('full_right', $personality->full_right) !!}
				</div>

				<div class="row">
					<div><p class="text-center">Imagen para la lista</p></div>
					<div class="row">
						<div class="col-xs-6">
							<img style="width: 100%;" src="{{asset($personality->image)}}">
						</div>
						<div class="col-xs-6">
						    {!! Form::label('Subir nueva imagen') !!}
						    {!! Form::file('image', null) !!}
						</div>
					</div>
				</div>
				<div class="row">
					<div><p class="text-center">Header para el detalle de personalidad</p></div>
					<div class="row">
						<div class="col-xs-6">
							<img style="width: 100%;" src="{{asset($personality->header)}}">
						</div>
						<div class="col-xs-6">
							{!! Form::label('Subir nueva imagen de header') !!}
						    {!! Form::file('header', null) !!}
						</div>
					</div>
				</div>

				<div class="row text-center" style="margin: 20px 0;">
					<a href="{{url('admin/personalidad/personalidades/' . $personality->id . '/slider')}}"><div class="btn btn-info">Administrar Slider de la personalidad</div></a>
				</div>

				<div class="form-group">
				</div>


				<div class="form-group">
					{!! Form::submit('Enviar') !!}
				</div>
				{!! Form::close() !!}

			</div>
		</div>
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
	$(function(){ 
		CKEDITOR.replace( 'description' );
		CKEDITOR.replace( 'extended' );
		CKEDITOR.replace( 'full_left' );
		CKEDITOR.replace( 'full_right' );
	})
</script>

@endsection