@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection
@section('contentheader_title')
	Quienes Somos - Item 1
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border text-center">
						<h3 class="box-title">Home</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
				{!! Form::open(['url' => 'admin/personalidad/update', 'files' => true]) !!}
				{!! Form::hidden('page_id' , $page->id) !!}
				{!! Form::hidden('redirect' , 'admin/personalidad/descripcion') !!}

				<!-- Quienes Somos Main Izquierda -->
				<div class="form-group">
					{!! Form::label('personalidad_main_title', 'Texto Header Personalidades de los arboles') !!}
					{!! Form::textArea('personalidad_main_title', $page->getMeta('personalidad_main_title')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('personalidad_single_title', 'Texto Header Personalidad de los arboles (Single Item') !!}
					{!! Form::textArea('personalidad_single_title', $page->getMeta('personalidad_single_title')) !!}
				</div>

				<div class="form-group">
					{!! Form::submit('Enviar') !!}
				</div>
				{!! Form::close() !!}

			</div>
		</div>
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
	$(function(){ 
		CKEDITOR.replace( 'personalidad_main_title' );
		CKEDITOR.replace( 'personalidad_single_title' );
	})
</script>

@endsection