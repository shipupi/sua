@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Admin
@endsection

@section('contentheader_title')
	Personalidades
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border text-center">
						<h3 class="box-title">Personalidades</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
				{!! Form::open(['url' => 'admin/personalidad/updateSlider', 'files' => true]) !!}
				{!! Form::hidden('page_id' , $page->id) !!}
				{!! Form::hidden('redirect' , 'admin/personalidad/slider') !!}
				{!! Form::hidden('field' , 'slider_top') !!}


				<div class="row">
					<div class="col-md-2">
						#
					</div>
					<div class="text-center col-md-2">
						Imagen
					</div>
					<div class="text-center col-md-2">
					 	Editar
					</div>
				</div>
				@foreach ($personalities as $personality)
				<div data-action="removeSlider" data-field="slider_top" data-index="{{$loop->index}}" class="slider-image row" style="margin: 10px 0; border-bottom: 1px solid black; padding: 20px 0;" >
					<div class="col-md-2" >
						{{$loop->iteration}}
					</div>
					<div class="col-md-2" >
						<img style="width: 100%;" src="{{asset($personality->image)}}">
					</div>
					<div class="col-md-2" >
						<a href="{{url('admin/personalidad/personalidades/' . $personality->id)}}"><span class="adm-btn">EDIT</span></a>
						<!-- <i style="cursor: pointer;" class='remove-image fa fa-close'></i> -->
					</div>
				</div>
				@endforeach

				{!! Form::close() !!}

			</div>
		</div>
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
	$(function() {
		$('body').on('click', '.remove-image', function() {
			var $this = $(this)
			var $row = $this.closest('.slider-image')
			var data  = {
				field: $row.data('field'),
				action: $row.data('action'),
				index: $row.data('index')
			}
			$.ajax({
				method: 'POST',
				url: "{{url('admin/personalidad/ajaxAction')}}",
				data: data
			}).done(function(data){
				if (data == "ok") {
					$row.remove()
				}
			})
		})
	})
</script>

@endsection