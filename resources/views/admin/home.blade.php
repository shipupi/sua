@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border text-center">
						<h3 class="box-title">Home</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
				{!! Form::open(['url' => 'admin/home', 'files' => true]) !!}
				{!! Form::hidden('page_id' , $home->id) !!}





				<!-- Slider -->
				<div class="form-group">
					{!! Form::label('quienes_somos_main_title', 'Archivos del slider') !!}
					<div class="slider_items">
					@foreach( json_decode($home->getMeta('slider_top')) as $slider_item)
						<div class="row">
							<div class="col-md-1">
								X
							</div>
							<div class="col-md-11">
								<img src="{{$slider_item}}">
							</div>
						</div>
					@endforeach
					</div>
					{!! Form::file('slider_image') !!}					
				</div>

				<!-- Quienes Somos Main Izquierda -->
				<div class="form-group">
					{!! Form::label('quienes_somos_main_title', 'Quienes somos izquierda titulo') !!}
					{!! Form::text('quienes_somos_main_title', $home->getMeta('quienes_somos_main_title')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('quienes_somos_main_body', 'Quienes somos izquierda body') !!}
					{!! Form::textarea('quienes_somos_main_body', $home->getMeta('quienes_somos_main_body')) !!}
				</div>

				<!-- Quienes Somos Item 1 -->
				<div class="form-group">
					{!! Form::label('quienes_somos_1_title', 'Quienes somos item 1 titulo') !!}
					{!! Form::text('quienes_somos_1_title', $home->getMeta('quienes_somos_1_title')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('quienes_somos_1_body', 'Quienes somos item 1 body') !!}
					{!! Form::textarea('quienes_somos_1_body', $home->getMeta('quienes_somos_1_body')) !!}
				</div>


				<!-- Quienes Somos Item 2 -->
				<div class="form-group">
					{!! Form::label('quienes_somos_2_title', 'Quienes somos item 2 titulo') !!}
					{!! Form::text('quienes_somos_2_title', $home->getMeta('quienes_somos_2_title')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('quienes_somos_2_body', 'Quienes somos item 2 body') !!}
					{!! Form::textarea('quienes_somos_2_body', $home->getMeta('quienes_somos_2_body')) !!}
				</div>


				<!-- Quienes Somos Item 3 -->
				<div class="form-group">
					{!! Form::label('quienes_somos_3_title', 'Quienes somos item 3 titulo') !!}
					{!! Form::text('quienes_somos_3_title', $home->getMeta('quienes_somos_3_title')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('quienes_somos_3_body', 'Quienes somos item 3 body') !!}
					{!! Form::textarea('quienes_somos_3_body', $home->getMeta('quienes_somos_3_body')) !!}
				</div>



				<div class="form-group">
					{!! Form::submit('Enviar') !!}
				</div>
				{!! Form::close() !!}

			</div>
		</div>
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
	$(function(){ 
		CKEDITOR.replace( 'quienes_somos_main_body' );
		CKEDITOR.replace( 'quienes_somos_1_body' );
		CKEDITOR.replace( 'quienes_somos_2_body' );
		CKEDITOR.replace( 'quienes_somos_3_body' );
	})
</script>

@endsection