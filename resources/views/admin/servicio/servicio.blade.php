@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection

@section('contentheader_title')
	Descripcion Servicio
@endsection
@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border text-center">
						<h3 class="box-title">Home</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
				{!! Form::open(['url' => 'admin/servicio/update', 'files' => true]) !!}
				{!! Form::hidden('page_id' , $page->id) !!}
				{!! Form::hidden('redirect' , 'admin/servicio/servicio') !!}

				<!-- Quienes Somos Main Izquierda -->
				<div class="form-group">
					{!! Form::label('servicio_desc_title', 'Descripcion Servicio - Titulo') !!}
					{!! Form::text('servicio_desc_title', $page->getMeta('servicio_desc_title')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('servicio_desc_body', 'Descripcion Servicio - Subtitulo') !!}
					{!! Form::textarea('servicio_desc_body', $page->getMeta('servicio_desc_body')) !!}
				</div>

				<div class="form-group">
					{!! Form::submit('Enviar') !!}
				</div>
				{!! Form::close() !!}

			</div>
		</div>
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
	$(function(){ 
		CKEDITOR.replace( 'servicio_desc_der_body' );
		CKEDITOR.replace( 'servicio_desc_izq_body' );
	})
</script>

@endsection