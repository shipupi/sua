@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection

@section('contentheader_title')
	Seccion Iconos
@endsection
@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border text-center">
						<h3 class="box-title">Home</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
				{!! Form::open(['url' => 'admin/servicio/update', 'files' => true]) !!}
				{!! Form::hidden('page_id' , $page->id) !!}
				{!! Form::hidden('redirect' , 'admin/servicio/video') !!}

				<!-- 1 -->
				<div class="form-group">
					{!! Form::label('servicios_video', 'ID De video de youutube') !!}
					{!! Form::text('servicios_video', $page->getMeta('servicios_video')) !!}
				</div>


				<div class="form-group">
					{!! Form::submit('Enviar') !!}
				</div>
				{!! Form::close() !!}

			</div>
		</div>
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
	$(function(){ 
		CKEDITOR.replace( 'iconos_1_body' );
		CKEDITOR.replace( 'iconos_2_body' );
		CKEDITOR.replace( 'iconos_3_body' );
		CKEDITOR.replace( 'iconos_4_body' );
		CKEDITOR.replace( 'iconos_5_body' );
		CKEDITOR.replace( 'iconos_6_body' );
	})
</script>

@endsection