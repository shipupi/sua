@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection

@section('contentheader_title')
	Seccion Iconos
@endsection
@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border text-center">
						<h3 class="box-title">Home</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
				{!! Form::open(['url' => 'admin/servicio/update', 'files' => true]) !!}
				{!! Form::hidden('page_id' , $page->id) !!}
				{!! Form::hidden('redirect' , 'admin/servicio/numerada') !!}

				<!-- 1 -->
				<div class="form-group">
					{!! Form::label('iconos_1_title', 'Icono 1 - Titulo') !!}
					{!! Form::text('iconos_1_title', $page->getMeta('iconos_1_title')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('iconos_1_body', 'Icono 1 - Contenido') !!}
					{!! Form::textarea('iconos_1_body', $page->getMeta('iconos_1_body')) !!}
				</div>
				<!-- /1 -->

				<!-- 2 -->
				<div class="form-group">
					{!! Form::label('iconos_2_title', 'Icono 2 - Titulo') !!}
					{!! Form::text('iconos_2_title', $page->getMeta('iconos_2_title')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('iconos_2_body', 'Icono 2 - Contenido') !!}
					{!! Form::textarea('iconos_2_body', $page->getMeta('iconos_2_body')) !!}
				</div>
				<!-- /2 -->

				<!-- 3 -->
				<div class="form-group">
					{!! Form::label('iconos_3_title', 'Icono 3 - Titulo') !!}
					{!! Form::text('iconos_3_title', $page->getMeta('iconos_3_title')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('iconos_3_body', 'Icono 3 - Contenido') !!}
					{!! Form::textarea('iconos_3_body', $page->getMeta('iconos_3_body')) !!}
				</div>
				<!-- /3 -->

				<!-- 4 -->
				<div class="form-group">
					{!! Form::label('iconos_4_title', 'Icono 4 - Titulo') !!}
					{!! Form::text('iconos_4_title', $page->getMeta('iconos_4_title')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('iconos_4_body', 'Icono 4 - Contenido') !!}
					{!! Form::textarea('iconos_4_body', $page->getMeta('iconos_4_body')) !!}
				</div>
				<!-- /4 -->

				<!-- 5 -->
				<div class="form-group">
					{!! Form::label('iconos_5_title', 'Icono 5 - Titulo') !!}
					{!! Form::text('iconos_5_title', $page->getMeta('iconos_5_title')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('iconos_5_body', 'Icono 5 - Contenido') !!}
					{!! Form::textarea('iconos_5_body', $page->getMeta('iconos_5_body')) !!}
				</div>
				<!-- /5 -->

				<!-- 6 -->
				<div class="form-group">
					{!! Form::label('iconos_6_title', 'Icono 6 - Titulo') !!}
					{!! Form::text('iconos_6_title', $page->getMeta('iconos_6_title')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('iconos_6_body', 'Icono 6 - Contenido') !!}
					{!! Form::textarea('iconos_6_body', $page->getMeta('iconos_6_body')) !!}
				</div>
				<!-- /6 -->



				<div class="form-group">
					{!! Form::submit('Enviar') !!}
				</div>
				{!! Form::close() !!}

			</div>
		</div>
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
	$(function(){ 
		CKEDITOR.replace( 'iconos_1_body' );
		CKEDITOR.replace( 'iconos_2_body' );
		CKEDITOR.replace( 'iconos_3_body' );
		CKEDITOR.replace( 'iconos_4_body' );
		CKEDITOR.replace( 'iconos_5_body' );
		CKEDITOR.replace( 'iconos_6_body' );
	})
</script>

@endsection