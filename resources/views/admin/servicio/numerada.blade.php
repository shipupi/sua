@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection

@section('contentheader_title')
	Seccion Numerada
@endsection
@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border text-center">
						<h3 class="box-title">Home</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
				{!! Form::open(['url' => 'admin/servicio/update', 'files' => true]) !!}
				{!! Form::hidden('page_id' , $page->id) !!}
				{!! Form::hidden('redirect' , 'admin/servicio/numerada') !!}

				<!-- Quienes Somos Main Izquierda -->
				<div class="form-group">
					{!! Form::label('seccion_numerada_title', 'Titulo') !!}
					{!! Form::text('seccion_numerada_title', $page->getMeta('seccion_numerada_title')) !!}
				</div>


				<div class="form-group">
					{!! Form::label('seccion_numerada_1_title', 'Item 1 - Titulo') !!}
					{!! Form::text('seccion_numerada_1_title', $page->getMeta('seccion_numerada_1_title')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('seccion_numerada_1_body', 'Item 1 - Contenido') !!}
					{!! Form::textarea('seccion_numerada_1_body', $page->getMeta('seccion_numerada_1_body')) !!}
				</div>


				<div class="form-group">
					{!! Form::label('seccion_numerada_2_title', 'Item 2 - Titulo') !!}
					{!! Form::text('seccion_numerada_2_title', $page->getMeta('seccion_numerada_2_title')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('seccion_numerada_2_body', 'Item 2 - Contenido') !!}
					{!! Form::textarea('seccion_numerada_2_body', $page->getMeta('seccion_numerada_2_body')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('seccion_numerada_3_title', 'Item 3 - Titulo') !!}
					{!! Form::text('seccion_numerada_3_title', $page->getMeta('seccion_numerada_3_title')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('seccion_numerada_3_body', 'Item 3 - Contenido') !!}
					{!! Form::textarea('seccion_numerada_3_body', $page->getMeta('seccion_numerada_3_body')) !!}
				</div>

				<div class="form-group">
					{!! Form::submit('Enviar') !!}
				</div>
				{!! Form::close() !!}

			</div>
		</div>
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
	$(function(){ 
		CKEDITOR.replace( 'seccion_numerada_1_body' );
		CKEDITOR.replace( 'seccion_numerada_2_body' );
		CKEDITOR.replace( 'seccion_numerada_3_body' );
	})
</script>

@endsection