@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection

@section('contentheader_title')
	Header
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border text-center">
						<h3 class="box-title">Home</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
				{!! Form::open(['url' => 'admin/home/updateSlider', 'files' => true]) !!}
				{!! Form::hidden('page_id' , $page->id) !!}
				{!! Form::hidden('redirect' , 'admin/home/slider') !!}
				{!! Form::hidden('field' , 'slider_top') !!}
				<div class="form-group">
					{!! Form::label('servicio_header_title', 'Titulo Header') !!}
					{!! Form::text('servicio_header_title', $page->getMeta('servicio_header_title')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('servicio_header_subtitle', 'Subtitulo Header') !!}
					{!! Form::text('servicio_header_subtitle', $page->getMeta('servicio_header_subtitle')) !!}
				</div>
				<div class="form-group">
				    {!! Form::label('Subir nueva imagen') !!}
				    {!! Form::file('image', null) !!}
				</div>

				<div class="form-group">
					{!! Form::submit('Enviar') !!}
				</div>

				<div class="row">
					<div class="col-md-2">
						Imagen
					</div>
					<div class="col-md-4">
					 	Titulo
					</div>
					<div class="col-md-4">
						Subtitulo
					</div>
					<div class="col-md-2">

					</div>
				</div>
				@foreach (json_decode($page->getMeta('slider_top')) as $item)
				<div data-action="removeSlider" data-field="slider_top" data-index="{{$loop->index}}" class="slider-image row" style="margin: 10px 0; border-bottom: 1px solid black; padding: 20px 0;" >
					<div class="col-md-2" >
						<img style="width: 100%;" src="{{asset($item->image)}}">
					</div>
					<div class="col-md-4">
						{{$item->title}}
					</div>
					<div class="col-md-4">
						{{$item->subtitle}}
					</div>
					<div class="col-md-2" >
						<span class="adm-btn">EDIT</span>
						<i style="cursor: pointer;" class='remove-image fa fa-close'></i>
					</div>
				</div>
				@endforeach

				{!! Form::close() !!}

			</div>
		</div>
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
	$(function() {
		$('body').on('click', '.remove-image', function() {
			var $this = $(this)
			var $row = $this.closest('.slider-image')
			var data  = {
				field: $row.data('field'),
				action: $row.data('action'),
				index: $row.data('index')
			}
			$.ajax({
				method: 'POST',
				url: "{{url('admin/home/ajaxAction')}}",
				data: data
			}).done(function(data){
				if (data == "ok") {
					$row.remove()
				}
			})
		})
	})
</script>

@endsection