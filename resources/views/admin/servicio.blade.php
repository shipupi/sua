@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Servicio
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border text-center">
						<h3 class="box-title">Servicio</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
				{!! Form::open(['url' => 'admin/servicio', 'files' => true]) !!}
				{!! Form::hidden('page_id' , $servicio->id) !!}



				<!-- Servicio descripcion Izquierda -->
				<div class="form-group">
					{!! Form::label('servicio_desc_izq_title', 'Titulo servicio descripcion izquierda') !!}
					{!! Form::text('servicio_desc_izq_title', $servicio->getMeta('servicio_desc_izq_title')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('servicio_desc_izq_body', 'Contenido servicio descripcion izquierda') !!}
					{!! Form::textarea('servicio_desc_izq_body', $servicio->getMeta('servicio_desc_izq_body')) !!}
				</div>

				



				<div class="form-group">
					{!! Form::submit('Enviar') !!}
				</div>
				{!! Form::close() !!}

			</div>
		</div>
	</div>
@endsection


@section('scripts_loaded')

@endsection