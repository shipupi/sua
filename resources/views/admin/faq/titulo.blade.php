@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection
@section('contentheader_title')
	Quienes Somos - Item 1
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border text-center">
						<h3 class="box-title">Home</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
				{!! Form::open(['url' => 'admin/faq/update', 'files' => true]) !!}
				{!! Form::hidden('page_id' , $page->id) !!}
				{!! Form::hidden('redirect' , 'admin/faq/titulo') !!}

				<!-- Quienes Somos Main Izquierda -->
				<div class="form-group">
					{!! Form::label('faq_header_title', 'Titulo Header') !!}
					{!! Form::text('faq_header_title', $page->getMeta('faq_header_title')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('faq_header_subtitle', 'Subtitulo Header') !!}
					{!! Form::text('faq_header_subtitle', $page->getMeta('faq_header_subtitle')) !!}
				</div>

				<div class="row">
					<div style="margin-bottom: 20px;"><p class="text-center">Header</p></div>
					<div class="row">
						<div class="col-xs-6">
							{!! Form::label('Subir nueva imagen de header') !!}
						    {!! Form::file('faq_header', null) !!}
						</div>
						<div class="col-xs-6">
							<img style="width: 100%;" src="{{asset($page->getMeta('faq_header'))}}">
						</div>
					</div>
				</div>

				<div class="form-group text-center" style="margin-top:30px;">
					{!! Form::submit('Enviar') !!}
				</div>
				{!! Form::close() !!}

			</div>
		</div>
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
</script>

@endsection