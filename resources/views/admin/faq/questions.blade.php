@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Preguntas Frecuentes
@endsection
@section('contentheader_title')
	Preguntas Frecuentes
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				{!! Form::open(['url' => 'admin/faq/update', 'files' => true]) !!}
				{!! Form::hidden('page_id' , $page->id) !!}
				{!! Form::hidden('redirect' , 'admin/faq/questions') !!}

				<!-- Quienes Somos Main Izquierda -->
				<div class="form-group">
					{!! Form::label('faq_1_title', 'Preguntas Frecuentes 1 - Titulo') !!}
					{!! Form::text('faq_1_title', $page->getMeta('faq_1_title')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('faq_1_body', 'Preguntas Frecuentes 1 - Contenido') !!}
					{!! Form::textarea('faq_1_body', $page->getMeta('faq_1_body')) !!}
				</div>


				<div class="form-group">
					{!! Form::label('faq_2_title', 'Preguntas Frecuentes 2 - Titulo') !!}
					{!! Form::text('faq_2_title', $page->getMeta('faq_2_title')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('faq_2_body', 'Preguntas Frecuentes 2 - Contenido') !!}
					{!! Form::textarea('faq_2_body', $page->getMeta('faq_2_body')) !!}
				</div>


				<div class="form-group">
					{!! Form::label('faq_3_title', 'Preguntas Frecuentes 3 - Titulo') !!}
					{!! Form::text('faq_3_title', $page->getMeta('faq_3_title')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('faq_3_body', 'Preguntas Frecuentes 3 - Contenido') !!}
					{!! Form::textarea('faq_3_body', $page->getMeta('faq_3_body')) !!}
				</div>


				<div class="form-group">
					{!! Form::label('faq_4_title', 'Preguntas Frecuentes 4 - Titulo') !!}
					{!! Form::text('faq_4_title', $page->getMeta('faq_4_title')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('faq_4_body', 'Preguntas Frecuentes 4 - Contenido') !!}
					{!! Form::textarea('faq_4_body', $page->getMeta('faq_4_body')) !!}
				</div>


				<div class="form-group">
					{!! Form::label('faq_5_title', 'Preguntas Frecuentes 5 - Titulo') !!}
					{!! Form::text('faq_5_title', $page->getMeta('faq_5_title')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('faq_5_body', 'Preguntas Frecuentes 5 - Contenido') !!}
					{!! Form::textarea('faq_5_body', $page->getMeta('faq_5_body')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('faq_6_title', 'Preguntas Frecuentes 6 - Titulo') !!}
					{!! Form::text('faq_6_title', $page->getMeta('faq_6_title')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('faq_6_body', 'Preguntas Frecuentes 6 - Contenido') !!}
					{!! Form::textarea('faq_6_body', $page->getMeta('faq_6_body')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('faq_7_title', 'Preguntas Frecuentes 7 - Titulo') !!}
					{!! Form::text('faq_7_title', $page->getMeta('faq_7_title')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('faq_7_body', 'Preguntas Frecuentes 7 - Contenido') !!}
					{!! Form::textarea('faq_7_body', $page->getMeta('faq_7_body')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('faq_8_title', 'Preguntas Frecuentes 8 - Titulo') !!}
					{!! Form::text('faq_8_title', $page->getMeta('faq_8_title')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('faq_8_body', 'Preguntas Frecuentes 8 - Contenido') !!}
					{!! Form::textarea('faq_8_body', $page->getMeta('faq_8_body')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('faq_9_title', 'Preguntas Frecuentes 9 - Titulo') !!}
					{!! Form::text('faq_9_title', $page->getMeta('faq_9_title')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('faq_9_body', 'Preguntas Frecuentes 9 - Contenido') !!}
					{!! Form::textarea('faq_9_body', $page->getMeta('faq_9_body')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('faq_10_title', 'Preguntas Frecuentes 10 - Titulo') !!}
					{!! Form::text('faq_10_title', $page->getMeta('faq_10_title')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('faq_10_body', 'Preguntas Frecuentes 10 - Contenido') !!}
					{!! Form::textarea('faq_10_body', $page->getMeta('faq_10_body')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('faq_11_title', 'Preguntas Frecuentes 11 - Titulo') !!}
					{!! Form::text('faq_11_title', $page->getMeta('faq_11_title')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('faq_11_body', 'Preguntas Frecuentes 11 - Contenido') !!}
					{!! Form::textarea('faq_11_body', $page->getMeta('faq_11_body')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('faq_12_title', 'Preguntas Frecuentes 12 - Titulo') !!}
					{!! Form::text('faq_12_title', $page->getMeta('faq_12_title')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('faq_12_body', 'Preguntas Frecuentes 12 - Contenido') !!}
					{!! Form::textarea('faq_12_body', $page->getMeta('faq_12_body')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('faq_13_title', 'Preguntas Frecuentes 13 - Titulo') !!}
					{!! Form::text('faq_13_title', $page->getMeta('faq_13_title')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('faq_13_body', 'Preguntas Frecuentes 13 - Contenido') !!}
					{!! Form::textarea('faq_13_body', $page->getMeta('faq_13_body')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('faq_14_title', 'Preguntas Frecuentes 14 - Titulo') !!}
					{!! Form::text('faq_14_title', $page->getMeta('faq_14_title')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('faq_14_body', 'Preguntas Frecuentes 14 - Contenido') !!}
					{!! Form::textarea('faq_14_body', $page->getMeta('faq_14_body')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('faq_15_title', 'Preguntas Frecuentes 15 - Titulo') !!}
					{!! Form::text('faq_15_title', $page->getMeta('faq_15_title')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('faq_15_body', 'Preguntas Frecuentes 15 - Contenido') !!}
					{!! Form::textarea('faq_15_body', $page->getMeta('faq_15_body')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('faq_16_title', 'Preguntas Frecuentes 16 - Titulo') !!}
					{!! Form::text('faq_16_title', $page->getMeta('faq_16_title')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('faq_16_body', 'Preguntas Frecuentes 16 - Contenido') !!}
					{!! Form::textarea('faq_16_body', $page->getMeta('faq_16_body')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('faq_17_title', 'Preguntas Frecuentes 17 - Titulo') !!}
					{!! Form::text('faq_17_title', $page->getMeta('faq_17_title')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('faq_17_body', 'Preguntas Frecuentes 17 - Contenido') !!}
					{!! Form::textarea('faq_17_body', $page->getMeta('faq_17_body')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('faq_18_title', 'Preguntas Frecuentes 18 - Titulo') !!}
					{!! Form::text('faq_18_title', $page->getMeta('faq_18_title')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('faq_18_body', 'Preguntas Frecuentes 18 - Contenido') !!}
					{!! Form::textarea('faq_18_body', $page->getMeta('faq_18_body')) !!}
				</div>


				<div class="form-group">
					{!! Form::submit('Enviar') !!}
				</div>
				{!! Form::close() !!}

			</div>
		</div>
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
	$(function(){ 
		CKEDITOR.replace( 'faq_1_body' );
		CKEDITOR.replace( 'faq_2_body' );
		CKEDITOR.replace( 'faq_3_body' );
		CKEDITOR.replace( 'faq_4_body' );
		CKEDITOR.replace( 'faq_5_body' );
		CKEDITOR.replace( 'faq_6_body' );
	})
</script>

@endsection