<!DOCTYPE html>
<html>
@section('head')
<head>
    <meta charset="utf-8" />
    <title>Seamos un arbol</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="nileforest">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

     <!-- Favicone Icon  -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- CSS -->
    <link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/font-awesome.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/ionicons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/plugin/jPushMenu.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/plugin/animate.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/plugin/slick.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/jquery-ui.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css" />

<!-- Google Ads -->
<script>

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){

(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),

m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)

})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-71839615-1', 'auto');

ga('send', 'pageview');

</script>


    

    <!-- youtube api -->
    <script>
      // 2. This code loads the IFrame Player API code asynchronously.
      var tag = document.createElement('script');
      var youtubePlayers = []
      var createYoutube, canSlide = true, homeSlider

      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

      // 3. This function creates an <iframe> (and YouTube player)
      //    after the API code downloads.
      function onYouTubeIframeAPIReady() {
        createYoutube = function(id, videoId) {
             var newPlayer = new YT.Player(id, {
              height: '390',
              width: '640',
              videoId: videoId,
              events: {
                // 'onReady': onPlayerReady,
                // 'onStateChange': onPlayerStateChange
              },
              playerVars: {
                'showinfo': 0,
                'controls': 0
            },
            });

            return newPlayer
        }
      }

      // 4. The API will call this function when the video player is ready.
      function onPlayerReady(event) {
        // event.target.playVideo();
      }

      // 5. The API calls this function when the player's state changes.
      //    The function indicates that when playing a video (state=1),
      //    the player should play for six seconds and then stop.
      var done = false;
      function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
          setTimeout(stopVideo, 6000);
          done = true;
        }
      }
      function stopVideo() {
        player.stopVideo();
      }
    </script>


</head>
@show
<body class="full-intro">

    <!-- Preloader -->
    <section id="preloader">
        <div class="loader" id="loader">
            <div class="loader-img"></div>
        </div>
    </section>
    <!-- End Preloader -->



    <!-- Search menu Top -->
    <section class=" top-search-bar cbp-spmenu cbp-spmenu-horizontal cbp-spmenu-top">
        <div class="container">
            <div class="search-wraper">
                <input type="text" class="input-sm form-full" placeholder="Search..." name="search" />
                <a class="search-bar-icon"><i class="fa fa-search"></i></a>
                <a class="bar-close toggle-menu menu-top push-body"><i class="ion ion-android-close"></i></a>
            </div>
        </div>
    </section>
    <!--End Search menu Top -->

    <!-- Site Wraper -->
    <div class="wrapper">

        <!-- HEADER -->
        @section('header')
        <header class="header">
            <div class="container-fluid">

                <!-- logo -->
                <!--End logo-->

                <!-- Rightside Menu (Search, Cart, Bart icon) -->
                <div class="side-menu-btn">
                    <ul>
                        <!-- Search Icon -->
                        <li class="">
                            <a class="right-icon menu-top" href="tel:08005550037">0800-555-0037</a>
                        </li>
                        <!-- End Search Icon -->
                    </ul>
                </div>
                <!-- End Rightside Menu -->

                <!-- Navigation Menu -->
                    <div class="logo-container">
                        <img src="{{asset('assets/master/logo-color.png')}}">
                    </div>
                    <nav class='navigation'>
                    <ul>
                        <li>
                            <a href="{{url('home')}}">HOME</a>
                        </li>

                        <li>
                            <a href="{{url('servicio')}}">NUESTRO SERVICIO</a>
                        </li>
                        <li>
                            <a href="{{url('personalidad')}}">ESPECIES QUE PLANTAMOS</a>
                        </li>
                        <li>
                            <a href="{{url('preguntas_frecuentes')}}">PREGUNTAS FRECUENTES</a>
                        </li>
                        <li>
                            <a href="{{url('contacto')}}">CONTACTO</a>
                        </li>

                    </ul>
                </nav>
                <!--End Navigation Menu -->

            </div>
        </header>
        @show
        <!-- END HEADER -->
        @section ('main')
        <!-- CONTENT-->
        @yield('content')
        <!-- Intro Section -->

        @show
        <!-- END CONTENT -->

        <!-- FOOTER -->
        @section('footer')
               <footer class="footer pt-80">
            <div class="container">
                <div class="row mb-60">
                    <!-- Logo -->
                    <div class="col-md-3 col-sm-3 col-xs-12 mb-xs-30">
                        <a class="footer-logo" href="/">
                            <img src="{{asset('assets/master/logo-grises.png')}}" /></a>
                    </div>
                    <!-- Logo -->

                    <!-- Newsletter -->
                    <!-- <div class="col-md-4 col-sm-5 col-xs-12 mb-xs-30">
                        <div class="newsletter">
                            <form>
                                <input type="email" class="newsletter-input input-md newsletter-input mb-0" placeholder="Enter Your Email">
                                <button class="newsletter-btn btn btn-xs btn-white" type="submit" value=""><i class="fa fa-angle-right mr-0"></i></button>
                            </form>
                        </div>
                    </div> -->
                    <!-- End Newsletter -->

                    <!-- Social -->
                    <div class="col-md-3 col-md-offset-6 col-sm-offset-5 col-sm-4 col-xs-12">
                        <div class="footer-contact direccion">R.P. 24 6164, Moreno, Buenos Aires</div>
                        <div class="footer-contact direccion">Diagonal Norte 1160, 5A, CABA.</div>
                        <div class="footer-contact telefono"><a href="tel:08005550037">0800-555-0037</a></div>
                    </div>
                    
                    <!-- End Social -->
                </div>
                <!--Footer Info -->
                <!-- End Footer Info -->
            </div>

            <hr />

            <!-- Copyright Bar -->
            <section class="copyright ptb-60">
                <div class="container">
                    <div class="col-md-6 col-sm-4 col-xs-12">
                        © {{date('Y')}} <a><b>Seamos un arbol</b></a>. Todos los derechos reservados.
                    </div>
                    <div class="col-md-3 col-md-offset-2 col-sm-4 col-xs-12">
                        <ul class="social">
                           <li><a target="_blank" href="https://www.facebook.com/Seamosunarbol"><i class="fa fa-facebook"></i></a></li>
                            <li><a target="_blank" href="https://www.instagram.com/seamosunarbol/"><i class="fa fa-instagram"></i></a></li>
                            <li><a target="_blank" href="https://www.youtube.com/channel/UCioGndIJ3xr0KFFOaTf52yA"><i class="fa fa-youtube"></i></a></li>
                        </ul>
                    </div>
                </div>
            </section>
            <!-- End Copyright Bar -->

        </footer>
        @show
        <!-- END FOOTER -->

        <!-- Scroll Top -->
        <a class="scroll-top">
            <i class="fa fa-angle-double-up"></i>
        </a>
        <!-- End Scroll Top -->

    </div>
    <!-- Site Wraper End -->


    <!-- JS -->

    <script type="text/javascript" src="{{ asset('assets/js/jquery-1.11.2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/jquery.flexslider.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/background-check.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/jquery.fitvids.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/jquery.viewportchecker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/jquery.stellar.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/wow.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/jquery.colorbox-min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/isotope.pkgd.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/masonry.pkgd.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/slick.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/imagesloaded.pkgd.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/jPushMenu.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/jquery.fs.tipper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/mediaelement-and-player.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/theme.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/navigation.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/app.js') }}"></script>
    <script>
    </script>

    @section ('scripts_loaded')
    <!-- CONTENT-->
        @yield('custom_scripts')
    @show
</body>
</html>
