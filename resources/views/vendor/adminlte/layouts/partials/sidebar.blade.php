<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}</a>
                </div>
            </div>
        @endif

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="{{ trans('adminlte_lang::message.search') }}..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
             <li class="{{isset($page->name)? $page->name == 'home'? 'active':'' : '' }} treeview">
                <a href="#">
                    <i class='fa fa-home'></i>
                    <span>Home</span> 
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{url('admin/home/slider')}}">Slider</a></li>
                    <li><a href="{{url('admin/home/quienesMain')}}">Quienes Somos Main</a></li>
                    <li><a href="{{url('admin/home/quienes1')}}">Quienes Somos 1</a></li>
                    <li><a href="{{url('admin/home/quienes2')}}">Quienes Somos 2</a></li>
                    <li><a href="{{url('admin/home/quienes3')}}">Quienes Somos 3</a></li>
                    <li><a href="{{url('admin/home/contadores')}}">Contadores</a></li>
                </ul>
            </li>

            <li class="{{isset($page->name)? $page->name == 'faq'? 'active':'' : '' }} treeview">
                <a href="#">
                    <i class='fa fa-question'></i>
                    <span>Preguntas Frecuentes</span> 
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{url('admin/faq/titulo')}}">Imagen Header</a></li>
                    <li><a href="{{url('admin/faq/questions')}}">Preguntas</a></li>
                </ul>
            </li>
            <li class="{{isset($page->name)? $page->name == 'servicio'? 'active':'' : '' }} treeview">
                <a href="#"><i class='fa fa-tree'></i> <span>Servicio</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <!-- <li><a href="{{url('admin/servicio/header')}}">Header</a></li> -->
                    <li><a href="{{url('admin/servicio/titulo')}}">Imagen Header</a></li>
                    <li><a href="{{url('admin/servicio/video')}}">Video</a></li>
                    <li><a href="{{url('admin/servicio/servicio')}}">Descripcion Servicio</a></li>
                    <li><a href="{{url('admin/servicio/slider')}}">Slider</a></li>
                    <li><a href="{{url('admin/servicio/numerada')}}">Seccion Numerada</a></li>
                    <li><a href="{{url('admin/servicio/iconos')}}">Seccion Iconos</a></li>
                </ul>
            </li>
            <li class="{{isset($page->name)? $page->name == 'personalidad'? 'active':'' : '' }} treeview">
                <a href="#"><i class='fa fa-smile-o'></i> <span>Personalidad</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{url('admin/personalidad/titulo')}}">Imagen Header</a></li>
                    <li><a href="{{url('admin/personalidad/descripcion')}}">Header</a></li>
                    <li><a href="{{url('admin/personalidad/personalidades')}}">Personalidades</a></li>
                </ul>
            </li>
            <li class="{{isset($page->name)? $page->name == 'contacto'? 'active':'' : '' }} treeview">
                <a href="#"><i class='fa fa-address-book-o'></i> <span>Contacto</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{url('admin/contacto/titulo')}}">Imagen Header</a></li>
                </ul>
            </li>

        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>