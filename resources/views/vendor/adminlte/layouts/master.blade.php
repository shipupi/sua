<!DOCTYPE html>
<html>
@section('head')
<head>
    <meta charset="utf-8" />
    <title>Seamos un arbol</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="nileforest">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

     <!-- Favicone Icon  -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">
    <!-- <link rel="icon" type="image/png" href="{{ asset('assets/img/favicon.png') }}"> -->
    <!-- <link rel="apple-touch-icon" href="{{ asset('assets/img/favicon.png') }}"> -->

<!--     <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/img/favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets/img/favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/img/favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/img/favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/img/favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/img/favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/img/favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/img/favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('assets/img/favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/img/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('assets/img/favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/img/favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('assets/img/favicon/manifest.json') }}"> -->
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- CSS -->
    <link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/font-awesome.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/ionicons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/plugin/jPushMenu.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/plugin/animate.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/jquery-ui.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css" />

</head>
@show
<body class="full-intro">

    <!-- Preloader -->
    <section id="preloader">
        <div class="loader" id="loader">
            <div class="loader-img"></div>
        </div>
    </section>
    <!-- End Preloader -->



    <!-- Search menu Top -->
    <section class=" top-search-bar cbp-spmenu cbp-spmenu-horizontal cbp-spmenu-top">
        <div class="container">
            <div class="search-wraper">
                <input type="text" class="input-sm form-full" placeholder="Search..." name="search" />
                <a class="search-bar-icon"><i class="fa fa-search"></i></a>
                <a class="bar-close toggle-menu menu-top push-body"><i class="ion ion-android-close"></i></a>
            </div>
        </div>
    </section>
    <!--End Search menu Top -->

    <!-- Site Wraper -->
    <div class="wrapper">

        <!-- HEADER -->
        @section('header')
        <header class="header">
            <div class="container">

                <!-- logo -->
                <!--End logo-->

                <!-- Rightside Menu (Search, Cart, Bart icon) -->
                <div class="side-menu-btn">
                    <ul>
                        <!-- Search Icon -->
                        <li class="">
                            <a class="right-icon search toggle-menu menu-top push-body"><i class="fa fa-search"></i></a>
                        </li>
                        <!-- End Search Icon -->
                    </ul>
                </div>
                <!-- End Rightside Menu -->

                <!-- Navigation Menu -->
                    <nav class='navigation'>
                    <div class="logo-container">
                        <img src="{{asset('assets/master/logo.jpg')}}">
                    </div>
                    <ul>
                        <li>
                            <a href="/">Home</a>
                        </li>

                        <li>
                            <a href="about">Informacion</a>
                        </li>
                        <li>
                            <a href="services">Personalidad</a>
                        </li>
                        <li>
                            <a href="pimienta">FAQ</a>
                        </li>
                        <li>
                            <a href="blog">Blog</a>
                        </li>
                        <li>
                            <a href="contact">Contacto</a>
                        </li>

                    </ul>
                </nav>
                <!--End Navigation Menu -->

            </div>
        </header>
        @show
        <!-- END HEADER -->
        @section ('main')
        <!-- CONTENT-->
        @yield('content')
        <!-- Intro Section -->

        @show
        <!-- END CONTENT -->

        <!-- FOOTER -->
        @section('footer')
               <footer class="footer pt-80">
            <div class="container">
                <div class="row mb-60">
                    <!-- Logo -->
                    <div class="col-md-3 col-sm-3 col-xs-12 mb-xs-30">
                        <a class="footer-logo" href="/">
                            <img src="{{asset('assets/master/logo.jpg')}}" /></a>
                    </div>
                    <!-- Logo -->

                    <!-- Newsletter -->
                    <div class="col-md-4 col-sm-5 col-xs-12 mb-xs-30">
                        <div class="newsletter">
                            <form>
                                <input type="email" class="newsletter-input input-md newsletter-input mb-0" placeholder="Enter Your Email">
                                <button class="newsletter-btn btn btn-xs btn-white" type="submit" value=""><i class="fa fa-angle-right mr-0"></i></button>
                            </form>
                        </div>
                    </div>
                    <!-- End Newsletter -->

                    <!-- Social -->
                    <div class="col-md-3 col-md-offset-2 col-sm-4 col-xs-12">
                        <ul class="social">
                        </ul>
                    </div>
                    <!-- End Social -->
                </div>
                <!--Footer Info -->
                <!-- End Footer Info -->
            </div>

            <hr />

            <!-- Copyright Bar -->
            <section class="copyright ptb-60">
                <div class="container">
                    <p class="">
                        © {{date('Y')}} <a><b>Seamos un arbol</b></a>. Todos los derechos reservados.
                        <br />

                    </p>
                </div>
            </section>
            <!-- End Copyright Bar -->

        </footer>
        @show
        <!-- END FOOTER -->

        <!-- Scroll Top -->
        <a class="scroll-top">
            <i class="fa fa-angle-double-up"></i>
        </a>
        <!-- End Scroll Top -->

    </div>
    <!-- Site Wraper End -->


    <!-- JS -->

    <script type="text/javascript" src="{{ asset('assets/js/jquery-1.11.2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/jquery.flexslider.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/background-check.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/jquery.fitvids.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/jquery.viewportchecker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/jquery.stellar.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/wow.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/jquery.colorbox-min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/isotope.pkgd.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/masonry.pkgd.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/imagesloaded.pkgd.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/jPushMenu.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/jquery.fs.tipper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugin/mediaelement-and-player.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/theme.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/navigation.js') }}"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript" src="{{ asset('assets/js/map.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/app.js') }}"></script>
    @section ('scripts_loaded')
    <!-- CONTENT-->
        @yield('custom_scripts')
    @show

</body>
</html>
