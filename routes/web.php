<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@home');
Route::get('servicio', 'PagesController@servicio');
Route::get('preguntas_frecuentes', 'PagesController@faq');
Route::get('personalidad', 'PagesController@personalidad');
Route::get('personalidad/{id}', 'PagesController@personalidadSingle');
Route::get('contacto', 'PagesController@contact');
Route::post('contact', 
  ['as' => 'contact_store', 'uses' => 'PagesController@contact_store']);



Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::group(['prefix' => 'home'], function () {
        Route::get('slider', 'Panel\HomeController@slider');
        Route::get('sliderSingle/{id}', 'Panel\HomeController@sliderSingle');
        Route::post('updateSingleSlider/{id}', 'Panel\HomeController@updateSliderSingle');
        Route::get('quienesMain', 'Panel\HomeController@quienesMain');
        Route::get('quienes1', 'Panel\HomeController@quienes1');
        Route::get('quienes2', 'Panel\HomeController@quienes2');
        Route::get('quienes3', 'Panel\HomeController@quienes3');
        Route::get('contadores', 'Panel\HomeController@contadores');
        Route::post('update', 'Panel\HomeController@update');
        Route::post('updateSlider', 'Panel\HomeController@updateSlider');
        Route::post('ajaxAction', 'Panel\HomeController@ajaxAction');
    });
    Route::group(['prefix' => 'servicio'], function () {
        Route::get('header', 'Panel\ServicioController@header');
        Route::get('titulo', 'Panel\ServicioController@titulo');
        Route::get('video', 'Panel\ServicioController@video');
        Route::get('servicio', 'Panel\ServicioController@servicio');
        Route::get('slider', 'Panel\ServicioController@slider');
        Route::get('numerada', 'Panel\ServicioController@numerada');
        Route::get('iconos', 'Panel\ServicioController@iconos');
        Route::post('update', 'Panel\ServicioController@update');
        Route::post('updateSlider', 'Panel\ServicioController@updateSlider');
        Route::post('ajaxAction', 'Panel\ServicioController@ajaxAction');
    });
    Route::group(['prefix' => 'contacto'], function () {
        Route::get('titulo', 'Panel\ContactoController@titulo');
        Route::post('update', 'Panel\ContactoController@update');
    });
    Route::group(['prefix' => 'faq'], function () {
        Route::get('titulo', 'Panel\FaqController@titulo');
        Route::get('questions', 'Panel\FaqController@questions');
        Route::post('update', 'Panel\FaqController@update');
    });
    Route::group(['prefix' => 'personalidad'], function () {
        Route::get('titulo', 'Panel\PersonalitiesController@titulo');
        Route::get('personalidades', 'Panel\PersonalitiesController@personalidades');
        Route::get('descripcion', 'Panel\PersonalitiesController@header');
        Route::get('personalidades/{id}', 'Panel\PersonalitiesController@personalidad');
        Route::get('personalidades/{id}/slider', 'Panel\PersonalitiesController@personalidadSlider');
        Route::post('personalidades/{id}/slider', 'Panel\PersonalitiesController@updateSlider');
        Route::post('updatePersonality', 'Panel\PersonalitiesController@updatePersonality');
        Route::post('update', 'Panel\PersonalitiesController@update');
        Route::post('ajaxAction', 'Panel\PersonalitiesController@ajaxAction');
    });


    Route::get('/', function() {
        return view('admin.default');
    });
    Route::get('{default}', function() {
        return view('admin.default');
    });
        

    Route::get('link1', function ()    {
       // Uses Auth Middleware
    });

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes
});