-- MySQL dump 10.13  Distrib 5.7.18, for Linux (x86_64)
--
-- Host: localhost    Database: sua
-- ------------------------------------------------------
-- Server version	5.7.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (12,'2014_10_12_000000_create_users_table',1),(13,'2014_10_12_100000_create_password_resets_table',1),(14,'2017_03_28_230643_create_home_page_table',1),(15,'2017_04_23_222626_create_personalities_table',1),(16,'2017_05_15_233556_add_slider_and_header_to_personalidades',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'home',NULL,NULL),(2,'servicio',NULL,NULL),(3,'faq',NULL,NULL),(4,'personalidad',NULL,NULL),(5,'contacto','2017-05-17 16:36:02','2017-05-17 16:36:02');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_meta`
--

DROP TABLE IF EXISTS `pages_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_meta` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_meta`
--

LOCK TABLES `pages_meta` WRITE;
/*!40000 ALTER TABLE `pages_meta` DISABLE KEYS */;
INSERT INTO `pages_meta` VALUES (1,1,'slider_top','[{\"image\":\"assets\\/home\\/slider\\/ceremonia 1.JPG\",\"title\":\"CEREMONIA DE PLANTADO\",\"subtitle\":null},{\"image\":\"assets\\/home\\/slider\\/home 2.png\",\"title\":\"CAMBIO DE PARADIGMA\",\"subtitle\":null},{\"image\":\"assets\\/home\\/slider\\/home 1.jpg\",\"title\":\"CEMENTERIOS BOSQUE\",\"subtitle\":null},{\"image\":\"assets\\/home\\/slider\\/ceremonia 2.JPG\",\"title\":\"RECUERDOS LLENOS DE VIDA\",\"subtitle\":null},{\"image\":\"t3aJNcJ4I_A\",\"title\":\"\",\"subtitle\":\"\"}]',NULL,'2017-05-31 01:28:03'),(2,1,'quienes_somos_main_title','seamos un árbol',NULL,'2017-04-25 14:35:18'),(3,1,'quienes_somos_main_body','<p>Seamos un &Aacute;rbol representa el <strong>cambio de paradigma</strong> en los servicios f&uacute;nebres: Transformar los cementerios tradicionales en nuevos <strong>cementerios bosque</strong>.</p>\r\n\r\n<p>A trav&eacute;s de las <strong>celebraciones de plantado</strong>, plantamos un &aacute;rbol en honor al difunto, sobre las cenizas de su cremaci&oacute;n. De esta manera, reemplazamos las antiguas l&aacute;pidas por majestuosos &aacute;rboles que honran la vida de los difuntos. Podremos ver crecer al &aacute;rbol, disfrutar del ecosistema que generan a su alrededor y adem&aacute;s, colaborar con la misi&oacute;n ben&eacute;fica que tienen los &aacute;rboles para el <strong>medio ambiente</strong>.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>',NULL,'2017-05-31 00:37:55'),(4,1,'quienes_somos_1_title','Nuevas ceremonias de plantado',NULL,'2017-04-24 12:36:24'),(5,1,'quienes_somos_1_body','<p>Nuestra objetivo es transformar las viejas ceremonias de sepelio en nuevas celebraciones de plantado. &nbsp;Los familiares y amigos se despiden de la persona fallecida <strong>plantando un &aacute;rbol</strong> para honrar al difunto. Es una revalorizaci&oacute;n de los restos y una forma de <strong>seguir generando vida</strong> luego del fallecimiento. El &aacute;rbol se nutrir&aacute; de las cenizas del difunto y transformar&aacute; su recuerdo en un <strong>monumento lleno de vida</strong>.</p>',NULL,'2017-05-10 19:44:44'),(6,1,'quienes_somos_2_title','Cementerios Bosque',NULL,'2017-04-24 12:36:55'),(7,1,'quienes_somos_2_body','<p>Convertimos los cementerios tradicionales en nuevos cementerios bosque, grandes <strong>espacios verdes llenos de vida</strong> que recuerdan y honran a nuestros seres queridos. Utilizamos los <strong>mismos recursos</strong> que en un sepelio tradicional pero los transformamos para colaborar con el medioambiente. Los nuevos cementerios funcionar&aacute;n como <strong>pulmones urbanos</strong> que mejorar&aacute;n la calidad de vida de la gente y ayudar&aacute;n a <strong>mitigar el cambio clim&aacute;tico</strong>.</p>',NULL,'2017-05-10 19:45:32'),(8,1,'quienes_somos_3_title','Cambio de paradigma',NULL,'2017-04-24 12:37:21'),(9,1,'quienes_somos_3_body','<p>Proponemos una <strong>resignificaci&oacute;n</strong> de lo que sucede con la muerte. Buscamos romper con las costumbres anacr&oacute;nicas y utilizar los mismos recursos de un servicio f&uacute;nebre tradicional, para plantar un &aacute;rbol y as&iacute; generar un <strong>bien com&uacute;n</strong>. Los servicios f&uacute;nebres pueden transformarse en nuevas ceremonias de plantado y la muerte puede ser el principio de una <strong>nueva forma de vida</strong>.</p>',NULL,'2017-05-10 19:46:29'),(10,2,'servicio_desc_izq_title','Servicio Tradicional',NULL,NULL),(11,2,'servicio_desc_izq_body','<p>Lorem ipsum servicio tradicional</p>',NULL,'2017-04-24 12:33:29'),(12,2,'servicio_desc_der_title','Nueva Mirada ecologica',NULL,'2017-04-24 18:39:46'),(13,2,'servicio_desc_der_body','<p>Seamos un &Aacute;rbol ofrece una soluci&oacute;n integral para aquellos familiares y amigos que se encuentran frente a la muerte de un ser querido. Un representante brindar&aacute; atenci&oacute;n personalizada y una soluci&oacute;n ajustada a cada caso. Comuniquese con nosotros las 24hs al 0800-555-0037.</p>',NULL,'2017-04-24 18:56:57'),(14,2,'seccion_numerada_title','Como funciona nuestro servicio',NULL,'2017-04-24 18:32:15'),(15,2,'seccion_numerada_1_title','Traslados y Trámites',NULL,'2017-04-24 18:32:15'),(16,2,'seccion_numerada_1_body','<p>Llamando al 0800-555-0037 un representante asesorar&aacute;&nbsp;y te brindar&aacute; una soluci&oacute;n personalizada. Se coordinar&aacute; para buscar&nbsp;al fallecido, realizar&nbsp;los tr&aacute;mites correspondientes y trasladarlo al crematorio.</p>',NULL,'2017-05-30 23:22:29'),(17,2,'seccion_numerada_2_title','Cremación',NULL,'2017-04-24 18:32:15'),(18,2,'seccion_numerada_2_body','<p>El personal de Seamos un &Aacute;rbol se ocupar&aacute; de gestionar los requerimientos para la cremaci&oacute;n para luego trasladar las cenizas al cementerio bosque.</p>',NULL,'2017-04-24 18:32:15'),(19,2,'seccion_numerada_3_title','Ceremonía de plantado',NULL,'2017-04-24 18:32:15'),(20,2,'seccion_numerada_3_body','<p>Se coordinar&aacute; con los&nbsp;familiares y amigos la ceremonia de plantando. Los mismos familiares podr&aacute;n ser portagonistas del plantado, enterrando las cenizas y sobre ellas el &aacute;rbol&nbsp;para honrar al difunto. Esta nueva ceremonia es una revalorizaci&oacute;n de los restos y una forma de seguir generando vida luego de la muerte.</p>',NULL,'2017-04-24 18:32:15'),(21,2,'servicio_slider','[\"assets\\/servicio\\/slider\\/Captura de pantalla 2017-05-26 a las 2.49.57 p.m..png\",\"assets\\/servicio\\/slider\\/slider servicio 2aa.png\",\"assets\\/servicio\\/slider\\/slider servicio 2 bueno.png\",\"assets\\/servicio\\/slider\\/slider servicio 21.png\",\"assets\\/servicio\\/slider\\/slider servicio 2.png\"]',NULL,'2017-05-31 01:21:12'),(22,2,'iconos_1_title','Servicios sin costos adicionales',NULL,NULL),(23,2,'iconos_1_body','<p>El precio es el informado, sin ning&uacute;n tipo de adicionales.&nbsp;</p>',NULL,'2017-04-24 18:36:15'),(24,2,'iconos_2_title','Resolvemos tu problema',NULL,NULL),(25,2,'iconos_2_body','<p>Llamanos las 24hs los 365 d&iacute;as del a&ntilde;o y te ayudaremos.</p>',NULL,'2017-04-24 18:36:15'),(26,2,'iconos_3_title','Sin costos de manutención',NULL,NULL),(27,2,'iconos_3_body','<p>El servicio no tiene costos de manutenci&oacute;n</p>',NULL,'2017-04-24 18:36:15'),(28,2,'iconos_4_title','Trabajamos con todas las cías de seguros',NULL,NULL),(29,2,'iconos_4_body','<p>Comunicate con nosotros para informarte sobre las coberturas.</p>',NULL,'2017-04-24 18:36:15'),(30,2,'iconos_5_title','Precio para independientes',NULL,NULL),(31,2,'iconos_5_body','<p>En caso de no contar con ning&uacute;n seguro o cobertura, tenemos una t&aacute;rifa especial para independientes.</p>',NULL,'2017-04-24 18:36:15'),(32,2,'iconos_6_title','Respaldo de S.F.I.',NULL,NULL),(33,2,'iconos_6_body','<p>Una empresa con m&aacute;s de 40 a&ntilde;os en el rubro funebre argentino.</p>',NULL,'2017-04-24 18:36:15'),(34,3,'faq_1_title','¿Qué inculye el servicio?',NULL,'2017-04-26 12:20:25'),(35,3,'faq_1_body','<p>El servicio incluye el traslado del fallecido, los tr&aacute;mites correspondientes, la cremaci&oacute;n&nbsp;y la ceremonia de plantado. El servicio no tienen gastos de manutenci&oacute;n a futuro.</p>',NULL,'2017-04-26 12:20:25'),(36,3,'faq_2_title','¿Dónde plantamos?',NULL,'2017-04-26 12:20:25'),(37,3,'faq_2_body','<p>Actualmente estamos plantando el primer cementerio bosque en el Parque del Buen Retiro, en Moreno, pcia. de Buenos Aires, que nos garantiza las condiciones necesarias para llevar adelante el proyecto</p>',NULL,'2017-04-26 12:20:25'),(38,3,'faq_3_title','Tengo una urgencia ¿Qué hago?',NULL,'2017-04-26 12:20:25'),(39,3,'faq_3_body','<p>Ll&aacute;menos al 0800-555-0037, nosotros nos encargaremos.</p>',NULL,'2017-04-26 12:20:25'),(40,3,'faq_4_title','¿Cuanto cuestan los servicios?',NULL,'2017-04-26 12:20:25'),(41,3,'faq_4_body','<p>Existen diferentes tipos de servicio.</p>\r\n\r\n<p>-El servicio f&uacute;nebre tradicional cuesta $21.500 pesos. Incluye Urgencia, Traslados, Documentaci&oacute;n, Sala velatoria y plantado del &Aacute;rbol en el bosque. En este servicio plantaremos un &aacute;rbol en conmemoraci&oacute;n en el bosque nativo de Tucum&aacute;n</p>\r\n\r\n<p>-El servicio de cremaci&oacute;n cuesta $18.000 pesos. Incluye Urgencia, Traslados, Documentaci&oacute;n, Cremaci&oacute;n y luego realizaremos una ceremonia de despedida y se planta un &aacute;rbol junto a familiares y amigos en el bosque que elijan.</p>\r\n\r\n<p>SUGERENCIA: Consulte por coberturas de obras sociales y sindicato. Llame al 0800-555-0037</p>',NULL,'2017-04-26 12:20:25'),(42,3,'faq_5_title','¿Se aceptan obras sociales u otras coberturas?',NULL,'2017-04-26 12:20:25'),(43,3,'faq_5_body','<p>Si. Consulte por su cobertura haciendo click aqu&iacute; (lista de seguros) o llame al 0800-555-0037. Tomamos todo tipo de seguros y obras sociales, gestionamos el pago y de haber diferencia se abona.</p>',NULL,'2017-04-26 12:20:25'),(44,3,'faq_6_title','¿Cómo puedo abonar el servicio?',NULL,'2017-04-26 12:20:25'),(45,3,'faq_6_body','<p>Se puede pagar en cuotas o en un pago.</p>\r\n\r\n<p>-por Internet via Mercado Pago.</p>\r\n\r\n<p>-por transferencia.</p>\r\n\r\n<p>-con cup&oacute;n de pago.</p>\r\n\r\n<p>-en efectivo.</p>\r\n\r\n<p>-con tarjeta de d&eacute;bito y cr&eacute;dito.</p>',NULL,'2017-04-26 18:52:57'),(46,4,'personalidad_main_title','<h1>Las especies que plantamos</h1>\r\n\r\n<p>Seamos un &Aacute;rbol planta 6 especies diferentes que podr&aacute;n ser elegidas por quien contrate el servicio</p>',NULL,'2017-05-10 19:35:40'),(47,4,'personalidad_single_title','<p>Amar el arbol es comprender la vida.</p>\r\n\r\n<p>Sali&oacute;&nbsp;de abajo de la tierra para mirar el sol, y compadecido de los p&aacute;jaros, abri&oacute;&nbsp;los brazos para protejerlos, y compadecido de los hombre, le da cuanto posee.</p>\r\n\r\n<p>Recibe cada mirada como una caricia, y cada gota de agua como un tesoro.</p>\r\n\r\n<p>Transunto del universo, es todo serenidad, belleza y armonia.</p>\r\n\r\n<p>Sabio que ense&ntilde;a en silencio, santo que con cada mano pide al cielo la bienaventuranza para todos, artesano y artista que trabaja dia y noche para convertirse &eacute;l mismo en una plegaria que asciende al cielo.</p>\r\n\r\n<p>&iexcl;Ense&ntilde;emos a todos a defender y propagar el arbol!</p>\r\n\r\n<p>Constancio C. Vigil</p>',NULL,'2017-05-10 19:35:40'),(48,2,'servicio_desc_title','Servicio fúnebre con una mirada ecológica','2017-05-16 13:04:10','2017-05-16 13:04:10'),(49,2,'servicio_desc_body','Seamos un árbol ofrece un servicio fúnebre con una mirada ecológica y un fin noble: Utilizamos las cenizas del difunto para plantar un árbol en su nombre. \r\nA través de la ceremonia de plantado, los familiares y amigos participarán en el proceso de plantar un árbol sobre las cenizas. El árbol, además de conmemorar a nuestro ser querido, tendrá una misión benéfica para el medio ambiente.','2017-05-16 13:04:10','2017-05-16 13:05:53'),(50,3,'faq_7_title','¿En qué consiste el servicio de cremación completo?','2017-05-16 19:45:58','2017-05-16 19:45:58'),(51,3,'faq_7_body','Resolvemos la urgencia de manera telefónica. Nos llamás y buscamos al fallecido, realizamos los trámites correspondientes, trasladamos a crematorio y llevamos los restos hacia el cementerio bosque. Realizamos una ceremonia de plantado del árbol. Pueden acompañar o no el proceso. En caso de que si, se pacta de antemano una fecha y horario para realizar la ceremonia de plantación del árbol.','2017-05-16 19:45:58','2017-05-16 19:45:58'),(52,3,'faq_8_title','¿Dónde quedan los cementerios bosque?','2017-05-16 19:45:58','2017-05-16 19:45:58'),(53,3,'faq_8_body','Actualmente estamos plantando dos bosques junto con todos los que eligen Seamos un Árbol al morir. Un Pulmón Urbano, ubicado al oeste de la ciudad de Buenos Aires, entre Pilar y Moreno, en el cruce de las rutas 24 (ex 197) y 25. Es dentro del cementerio privado Parque del Buen Retiro. (http://www.parquedelbuenretiro.com.ar) \r\nEl otro bosque, enriqueciendo el Bosque Nativo, es en Tucumán, Buruyacú. A una hora de la ciudad, hacia el noroeste.\r\nAclaración: En ninguno de los dos bosques hay que pagar mantenimiento, ni tampoco ningún gasto extra. Seamos un Árbol generó una forma simple y digna de despedir a un ser querido con la generación de vida.','2017-05-16 19:45:58','2017-05-16 19:45:58'),(54,3,'faq_9_title','¿Qué árboles se pueden plantar?','2017-05-16 19:45:58','2017-05-16 19:45:58'),(55,3,'faq_9_body','Actualemente el servicio posiblita la elección entre 6 especies diferentes de árboles. Árboles ornamentales como Álamo Criollo, Gingko Biloba y Liquidambar.\r\nY árboles nativos como el Aguaribay, Ibrá Pitá y Jacarandá.','2017-05-16 19:45:58','2017-05-16 19:45:58'),(56,3,'faq_10_title','¿Se puede pagar el servicio de forma previsional?','2017-05-16 19:45:58','2017-05-16 19:45:58'),(57,3,'faq_10_body','Si se puede. Contamos con la posibilidad de abonar cualquiera de los servicios que ofrecemos en cuotas, por internet o en efectivo. Contamos con financiación de 3, 6 y hasta 12 cuotas. Si desea realizar un plan diferente, consulte y buscaremos ajustarnos a su necesidad.','2017-05-16 19:45:58','2017-05-16 19:45:58'),(58,3,'faq_11_title','¿Los bosques se pueden visitar?','2017-05-16 19:45:58','2017-05-16 19:45:58'),(59,3,'faq_11_body','Si se puede. Los dos bosques están abiertos los 365 días del año para que familiares y amigos puedan visitar, conmemorar, recordar y generar nuevos recuerdos.','2017-05-16 19:45:58','2017-05-16 19:45:58'),(60,3,'faq_12_title','¿Qué pasa si el árbol se enferma o muere?','2017-05-16 19:45:58','2017-05-16 19:45:58'),(61,3,'faq_12_body','El árbol es un ser vivo, puede enfermar y/o morir. Seamos un Árbol lo reemplaza para volver a dar vida en honor al difunto.','2017-05-16 19:45:58','2017-05-16 19:45:58'),(62,3,'faq_13_title','Si ya tengo cenizas. ¿Puedo hacerlas un árbol?','2017-05-18 13:43:37','2017-05-18 13:43:37'),(63,3,'faq_13_body','Si. por teléfono o vía e-mail coordinamos una fecha y horario para realizar la ceremonia de plantación en el bosque que elijas. Podrán visitar el bosque los 365 días del año. El costo es de $10.000 final. No hay gastos extras ni tampoco de mantenimiento.','2017-05-18 13:43:37','2017-05-18 13:43:37'),(64,3,'faq_14_title','¿Se puede plantar un árbol en conmemoración, sin cenizas?','2017-05-18 13:43:37','2017-05-18 13:43:37'),(65,3,'faq_14_body','Si, por teléfono o vía e-mail coordinamos una fecha y horario para la ceremonia de plantación en el bosque que elijas. Podrán visitar el bosque los 365 días del año. El costo es de $10.000 final. No hay gastos extras ni tampoco de mantenimiento.','2017-05-18 13:43:37','2017-05-18 13:43:37'),(66,3,'faq_15_title','¿Cómo es la ceremonia de plantado del árbol?','2017-05-18 13:43:37','2017-05-18 13:43:37'),(67,3,'faq_15_body','Los esperamos en el bosque urbano en la fecha y horario pactado y todo preparado. Vamos caminando hacia el lugar de la plantación, un espacio verde abierto con gran arboleda.\r\nNos tomamos el tiempo que los familiares deseen, luego vertemos las cenizas, rellenamos con tierra, y plantamos el árbol. Si los familiares desean pueden verter las cenizas y ayudar con la tierra. Finalizada la plantación, se entrega una regadera y una etiqueta donde escribirán lo que deseen. Funcionará a modo de etiqueta hasta que el árbol haya crecido y se le pueda colocar una insignia metálica.\r\nLa ceremonia podrá realizarse los 365 días del año entre las 8 y 16 hs. El árbol estará creciendo, generando y celebrando la vida.','2017-05-18 13:43:37','2017-05-18 13:43:37'),(68,3,'faq_16_title','¿Hay algún costo de mantenimiento?','2017-05-18 13:43:37','2017-05-18 13:43:37'),(69,3,'faq_16_body','No hay costo de mantenimiento.','2017-05-18 13:43:37','2017-05-18 13:43:37'),(70,3,'faq_17_title','¿Ofrecen servicio fúnebre con parcela?','2017-05-18 13:43:37','2017-05-18 13:43:37'),(71,3,'faq_17_body','El servicio fúnebre tradicional con parcela en cementerio privado cuesta $39.900\r\nResolvemos la urgencia, los traslados, la documentación y trasladamos al cementerio Parque del Buen Retiro donde se inhumará en una parcela. Además en este servicio nosotros plantaremos un árbol en conmemoración del fallecido en el bosque nativo de Tucumán.\r\nObservación: El árbol que plantamos no tiene gastos de mantenimiento ni nada a futuro. No obstante, el cementerio privado si posee gastos de mantenimiento a perpetuidad.','2017-05-18 13:43:37','2017-05-18 13:43:37'),(72,3,'faq_18_title','¿Tengo familiares en bóveda / Nicho y quisiera hacerlos un árbol, se puede?','2017-05-18 13:43:37','2017-05-18 13:43:37'),(73,3,'faq_18_body','Si. Realizamos el servicio completo, buscamos los ataúdes, los trasladamos a crematorio, los cremamos, llevamos las cenizas al cementerio bosque que elijas, y se realiza la plantación en la fecha y horario pactada.','2017-05-18 13:43:37','2017-05-18 13:43:37'),(74,3,'faq_header','assets/faq/cc -7486.jpg','2017-05-18 13:45:28','2017-05-18 13:45:28'),(75,3,'faq_header_title','Nuestro Servicio','2017-05-18 13:45:28','2017-05-18 13:45:28'),(76,3,'faq_header_subtitle','Servicio personalizado a las necesidades particulares','2017-05-18 13:45:28','2017-05-18 13:45:28'),(77,2,'servicio_header','assets/servicio/servicio 1.png','2017-05-19 15:39:39','2017-05-25 15:52:32'),(78,2,'servicio_header_title','','2017-05-19 15:39:39','2017-05-19 15:39:39'),(79,2,'servicio_header_subtitle','','2017-05-19 15:39:39','2017-05-19 15:39:39'),(80,4,'personalidad_header','assets/personalidades_header/IMG_2938.jpg','2017-05-21 14:07:11','2017-05-21 14:07:11'),(81,4,'personalidad_header_title','','2017-05-21 14:07:11','2017-05-21 14:07:11'),(82,4,'personalidad_header_subtitle','','2017-05-21 14:07:11','2017-05-21 14:07:11');
/*!40000 ALTER TABLE `pages_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personalities`
--

DROP TABLE IF EXISTS `personalities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personalities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `extended` longtext COLLATE utf8mb4_unicode_ci,
  `full_left` longtext COLLATE utf8mb4_unicode_ci,
  `full_right` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slider_images` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `header` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personalities`
--

LOCK TABLES `personalities` WRITE;
/*!40000 ALTER TABLE `personalities` DISABLE KEYS */;
INSERT INTO `personalities` VALUES (1,'assets/personalities/aguaribay.jpg','<h3><strong>AGUARIBAY</strong></h3>\r\n\r\n<p><em>Este &aacute;rbol puede alacanzar entre los quince y veinte metros de altura con apreciables caracter&iacute;sticas ornamentales.</em></p>','<p><strong>AGUARIBAY</strong></p>\r\n\r\n<p>Dicen que al dormir debajo de un aguaribay se tiene un sue&ntilde;o sereno. Un aroma suave inunda los sentidos, la brisa acaricia con su enramada. Adem&aacute;s &eacute;l aleja a todos los insectos para que su hu&eacute;sped tenga un reposo calmo a la sombra de su copa.</p>\r\n\r\n<p><strong>&nbsp;</strong></p>','<p><strong>AGUARIBAY</strong></p>\r\n\r\n<p><strong>Familia:</strong> Anacardiaceas</p>\r\n\r\n<p><strong>Nombre cient&iacute;fico:</strong> Schinus molle</p>\r\n\r\n<p><strong>Origen:</strong> Sudamerica</p>\r\n\r\n<p><strong>Nombre com&uacute;n:</strong> B&aacute;lsamo, gualeguay, terebinto, &aacute;rbol de la pimienta</p>\r\n\r\n<p>Este &aacute;rbol puede alacanzar entre los quince y veinte metros de altura con apreciables caracter&iacute;sticas ornamentales. Su copa esta formada por ramazones flexibles, p&eacute;ndulas, de follaje persistente, de un colorido verde gris&aacute;ceo.</p>\r\n\r\n<p>Las flores, peque&ntilde;as y amarillentas, se disponen en pan&iacute;culas terminales, auqnue tambi&eacute;n axilares, apartir del mes de diciembre. Los frutos son peque&ntilde;as drupas esf&eacute;ricas de cascara cori&aacute;cea y pulpa escasa que encierra un carozo dulce picante, que es empleado como sustituto de la pimienta.</p>','<p>Los incas lo consideraron sarado y lo llamaron el &aacute;rbol de la vida. Tambien lo denominaron mulli y era apreciado por los multiples usos que brindaba. Fue plantado a la vera de las rutas de comunicaci&oacute;n con el imperio Incaico, para dar protecci&oacute;n a los chasquis.</p>\r\n\r\n<p>Es un &aacute;rbol benefactor por su fortaleza, la frescura y la belleza que brinda desde su fuste, ramas y flores, adem&aacute;s de las multiples propiedades que le confieren. Dicen que al dormir debajo de un aguaribay se tiene un sue&ntilde;o sereno. Un aroma suave inunda los sentidos, la brisa acaricia con su enramada, adem&aacute;s &eacute;l aleja a todos los insectos para que su hu&eacute;sped tenga un reposo calmo a la sombra de su copa.</p>',NULL,'2017-05-30 14:56:43','[\"assets\\/personalities\\/slider\\/aguaribay 1.png\",\"assets\\/personalities\\/slider\\/aguaribay 2.png\",\"assets\\/personalities\\/slider\\/Aguaribay porte.jpg\"]','assets/personalities/Aguaribay porte.jpg'),(2,'assets/personalities/ibra pita.jpg','<h3>Ibir&aacute; Pit&aacute;</h3>\r\n\r\n<p><em>Su nombre com&uacute;n proviene del Guaran&iacute;,&nbsp;significa &aacute;rbol o madera roja por los variados tintes rojizos de su madera.</em></p>','<h3>Ibir&aacute; Pit&aacute;</h3>\r\n\r\n<p>Las vistosas flores amarillas que sobresalen por arriba de sus copas, la elegancia de su fuste y las caracter&iacute;sticas de su follaje hacen del Ibir&aacute; Pit&aacute; un &aacute;rbol muy vistoso.</p>\r\n\r\n<p>&nbsp;</p>','<h3>Ibir&aacute; Pit&aacute;</h3>\r\n\r\n<p><strong>Familia:</strong> Fab&aacute;ceas</p>\r\n\r\n<p><strong>Nombre cient&iacute;fico:</strong> Pelthophorum dubium</p>\r\n\r\n<p><strong>Origen: </strong>Norte de Argenentina, Sur de Brasil, Paraguay, nordeste de Uruguay.</p>\r\n\r\n<p><strong>Nombre com&uacute;n:</strong> Ibir&aacute; pit&aacute;, Ca&ntilde;af&iacute;stula, &Aacute;bol de Artigas (en Uruguay).</p>\r\n\r\n<p>Es un &aacute;rbol de gran porte que suele sobrepasar el dosel de las selvas de nuestro nordeste, alcanzando alturas que sobrepasan los veinticinco metros. Su copa es globosa e irregular. El tronco es derecho, cubierto con una corteza negra.</p>\r\n\r\n<p>El follaje semicaduco de su copa es muy parecido al del Jacarand&aacute;, raz&oacute;n por la cual en Buenos Aires de lo suele llamar incorrectamente Jacarand&aacute;&nbsp;de flor amarilla.</p>\r\n\r\n<p>Sus hojas son grandes, de treinta a cincuenta cent&iacute;metros de largo por quince a veinte cent&iacute;metros de ancho, compuestas de siete a veintiun pares de pinas de cuatro a diez cent&iacute;metros de largo, en las que se asientan los foliolos de seis a treinta pares, de un color verde brillante en la cara superior, opacos en el env&eacute;s, de forma el&iacute;ptica oblonga, opuestos, de cinco a diez mil&iacute;metros de largo.</p>','<p>Las flores hermafroditas est&aacute;n reunidas en racimos de largas panojas que superan a las hojas, teniendo un vistoso color amarillo, que lucen entre los mese de diciembre y abril.</p>\r\n\r\n<p>Los frutos indehiscentes tienen forma de vaina, son chatos, cori&aacute;ceos, alargados de seis a ocho cent&iacute;metros de largo por un centrimetro dy medio de ancho, donde contienen de una a tres semillas verduscas, lisas y muy duras.</p>\r\n\r\n<p>Por la elegancia de su fuste, las caracter&iacute;sticas de su follaje, que lo pierde tard&iacute;amente y los ramilletes conicos de vistosas flores amarillas, que sobresalen por arriba de sus copas en los meses de diciembre a abril, tiene un destacado valor ornamental y propicio para ser incorporado en parques.</p>',NULL,'2017-05-26 13:56:47','[\"assets\\/personalities\\/slider\\/ibra 1.png\",\"assets\\/personalities\\/slider\\/ibra 2.png\",\"assets\\/personalities\\/slider\\/ibra 3.png\"]','assets/personalities/ibira flor1.jpg'),(3,'assets/personalities/jacaranda-1.jpg','<h3>Jacarand&aacute;</h3>\r\n\r\n<p>Es uno&nbsp;de los m&aacute;s bellos&nbsp;por el color de sus flores viol&aacute;ceas&nbsp;que lo cubren totalmente&nbsp;antes de la floraci&oacute;n.</p>','<h3><strong>Jacarand&aacute;</strong></h3>\r\n\r\n<p>Dentro de nuestras especies aut&oacute;ctonas, uno de los mas bellos &aacute;rboles por el abundante e incomparables color de sus flores lavanda viol&aacute;ceas que lo cubren totalmente en noviembre antes de la floraci&oacute;n.</p>','<h3>Jacarand&aacute;</h3>\r\n\r\n<p>Este &aacute;rbol pude llegar en su h&aacute;bitat natural a sobrepasar los veinticinco metros de altura. Dentro de nuestras especies aut&oacute;ctonas, uno de los mas bellos &aacute;rboles por el abundante e incomparables color de sus flores lavanda viol&aacute;ceas que lo cubren totalmente en noviembre antes de la floraci&oacute;n. El segundo brote aparece a comienzos del oto&ntilde;o, cuando ya esta con todo su follaje, raz&oacute;n por la cual las flores no lucen como en primavera.</p>','<p>Su abundante floraci&oacute;n, en primavera lo vuelvo uno de los arboles originarios mas bellos. Las flores se agrupan en un pan&iacute;culo terminal de unos treinta cent&iacute;metros de largo que contiene el fruto en una capsula, de forma circular, de unos ocho cent&iacute;metros de di&aacute;metro donde conserva rosas semillas aladas.</p>',NULL,'2017-05-26 13:50:22','[\"assets\\/personalities\\/slider\\/jacaranda1.png\",\"assets\\/personalities\\/slider\\/jacaranda 2.png\",\"assets\\/personalities\\/slider\\/jacaranda 4.png\",\"assets\\/personalities\\/slider\\/jacaranda 5.png\"]','assets/personalities/jaca porte.jpeg'),(4,'assets/personalities/alamo.jpg','<h3>&Aacute;lamo Criollo</h3>\r\n\r\n<p><em>Es caracter&iacute;stico su fuste cil&iacute;ndrico, comprimido, sin ramaje colgante. Puede alcanzar los teinta y cinco metros de altura. </em></p>','<h3><strong>&Aacute;lamo Criollo</strong></h3>\r\n\r\n<p>Es un &aacute;rbol muy resistente a los vientos y a las bajas temperaturas. De los primeros en brotar en primavera, se destaca por el sonido apaciguador que genera el bailar de sus hojas frente a una leve brisa.</p>\r\n\r\n<p>&nbsp;</p>','<h3>&Aacute;lamo Criollo</h3>\r\n\r\n<p><strong>Familia:</strong> Salic&aacute;ceas</p>\r\n\r\n<p><strong>Nombre cient&iacute;fico:</strong> Populus nigra (alamo negro)</p>\r\n\r\n<p><strong>Origen: </strong>Asia central</p>\r\n\r\n<p><strong>Nombre com&uacute;n:</strong>&nbsp;&Aacute;lamo Criollo, &Aacute;lamo Negro de Lombardia, &Aacute;lamo Piramidal.</p>\r\n\r\n<p>Es caracter&iacute;stico su fuste cil&iacute;ndrico, comprimido, sin ramaje colgante. Puede alcanzar los teinta y cinco metros de altura. Es una especie muy resistente a las bajas temperaturas y a los fueres vientos.</p>','<p>Son muchas las especies conocidas del genero Populus. Todas tienen en com&uacute;n el movimiento de sus hojas ante las mas leve brisa por su forma aovada, sostenida por un largo pec&iacute;olo.</p>\r\n\r\n<p>Para los griegos, el follaje del &aacute;lamo blanco, con sus dos caras diferentes, oscuro de un lado y claro del otro, representa la dualidad de todo ser. Como el &aacute;rbol de la vida, el verde es la vida y su lado oscuro, la muerte. Tambi&eacute;n es un &aacute;rbol funerario.</p>',NULL,'2017-05-26 14:05:25','[\"assets\\/personalities\\/slider\\/alamo 1.png\",\"assets\\/personalities\\/slider\\/alamo 2.png\",\"assets\\/personalities\\/slider\\/alamo 3.png\",\"assets\\/personalities\\/slider\\/alamo 4.png\"]','assets/personalities/alamo hoja.jpg'),(5,'assets/personalities/gingko.jpeg','<h3>Gingko Biloba</h3>\r\n\r\n<p>Se considera al ginkgo el &aacute;rbol mas viejo del mundo.</p>','<p>Gingko Biloba</p>\r\n\r\n<p>Apodado el &aacute;rbol de la vida. Es longevo, fuerte y posee una belleza admirable en todos sus aspectos. De forma piramidal, con un follaje caduco, se transforma del verde vivo en verano a un dorado de fantas&iacute;as en oto&ntilde;o.</p>','<h3>Gingko Biloba</h3>\r\n\r\n<p><strong>Familia:</strong> Ginkgo&aacute;ceas</p>\r\n\r\n<p><strong>Nombre cient&iacute;fico:</strong> Ginkgo biloba</p>\r\n\r\n<p><strong>Origen:</strong> China y Jap&oacute;n</p>\r\n\r\n<p><strong>Nombre com&uacute;n:</strong> &Aacute;rbol de los cuarenta escudos, &Aacute;rbol de la vida.</p>\r\n\r\n<p>Se considera al ginkgo el &aacute;rbol mas viejo del mundo; &uacute;nico representante de una familia que vivio hace doscientos millones de a&ntilde;os, los restos fosiles encontrados demuestran que convivio con dinosaurios. Es originario de la regi&oacute;n del este de China, desde donde fue llevado a Japon. Para las religiones orientales es un &aacute;rbol sagrado.</p>\r\n\r\n<p>El ginkgo, palabra japonesa que quiere decir fruro de plata, es el &uacute;nico representante actual de una poblaci&oacute;n muy extendida en los periodos jur&aacute;sico y cret&aacute;cico (hace 180 millones de a&ntilde;os), de all&iacute; que por los restos fosiles encontrados de la era mesozoica se lo considere fosil viviente.</p>\r\n\r\n<p>Se cuenta que ocho mese despu&eacute;s que la bomba at&oacute;mica destruyo Hiroshima, el 6 de agosto de 1945, entre restos de cenizas y escombros asomaba un rete&ntilde;o vivo, que era de un magnifico &aacute;rbol buscando la luz y el aire, donde todo era desolaci&oacute;n y muerte. Este rete&ntilde;o tiene hoy diecis&eacute;is metros de altura y es considerado s&iacute;mbolo de la paz y esperanza. El tempo fue reconstruido con las escaleras de acceso modificadas, formando una U, para mantener intacto el &aacute;rbol sobreviviente. En su pie puede leerse. No mas Hiroshima.</p>','<p>En China y japon se conocen ginkgos de mas de mil a&ntilde;os.</p>\r\n\r\n<p>Su follaje caduco esta conformado por hojas que se parecen a un abanico, sostenidas por un largo peciolo y recorridas longitudinalmente por numerosas nervaduras. Son plantas dioicas, es decir, los sexos est&aacute;n separados en pies distintos. Sus flores son peque&ntilde;as tanto en uno como en otro sexo. El furto es una drupa de color amarillento del tama&ntilde;o de una peque&ntilde;a ciruela, con un desagradable olor cuando esta maduros. Recien entre los quince y veinte a&ntilde;os de edad tiene lugar la floraci&oacute;n, siendo entonces cuando se puede determinar el sexo. Las semillas son comestibles en China, donde se venden con el nombre de Paikwo y son muy empleadas para asarlas.</p>\r\n\r\n<p>Son &aacute;rboles de crecimiento lento, muy llamativos y decorativos no tan solo por el colorido amarillo intenso que toman sus hojas en el oto&ntilde;o, antes de caer, sino tambi&eacute;n por la elegancia de su fuste.</p>\r\n\r\n<p>Por su pausado desarrollo, en china se lo llama el &aacute;rbol del abuelo y del nieto (Koung Choun Chou). Saben que los frutos ser&aacute;n aprovechados por las generaciones futuras de quienes los plantan.</p>\r\n\r\n<p>Es un &aacute;rbol resistente a todas las pestes y que al sobrevivir las vicisitudes de cientos de millones de a&ntilde;os, evidentemente ha sido elegido por la madre naturaleza como el mas resistente, entre otras miles de familias existentes.</p>',NULL,'2017-05-26 14:12:03','[\"assets\\/personalities\\/slider\\/gingko 1.png\",\"assets\\/personalities\\/slider\\/ginkgo 2.png\",\"assets\\/personalities\\/slider\\/gingko 3.png\",\"assets\\/personalities\\/slider\\/gingko 4.png\",\"assets\\/personalities\\/slider\\/gingko 5.png\"]','assets/personalities/color en otoño.jpg'),(6,'assets/personalities/arbol icono.png','<h3>PROXIMAMENTE</h3>','<p>personalidad 6 descripcion extendida</p>','<p>texto de personalidad lado izquierdo</p>','<p>texto personalidad lado derecho</p>',NULL,'2017-05-24 18:28:33','[\"assets\\/personalities\\/slider\\/home2.png\",\"assets\\/personalities\\/slider\\/home2.png\"]','assets/personalities/home2.png');
/*!40000 ALTER TABLE `personalities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'brian','ail.brianez@gmail.com','$2y$10$/mDhs.wCWHdIKikxmge0euSrwvfc2qrduPRdcpGkdo7mwC0s7K1h6','ceNPRx8WCNapdAjqz73vCTt9MR0hS1yIEaPvHTNi2QPjaV7NzhQRNT2IJeJX','2017-04-24 02:27:55','2017-04-24 02:27:55'),(2,'Eke','esnaola.ef@gmail.com','$2y$10$WJUkNznycpz6p7XtTtRK0uoTXL1diR9RGNwrnZLAbTsHRhyYjpyHW','sncyjR872eyYzxGdZTdNy6qjLW789U4jMyrUGv1OCg3cj7LxVX1dthu41usy','2017-04-24 02:27:55','2017-04-24 02:27:55');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-31  2:02:27
